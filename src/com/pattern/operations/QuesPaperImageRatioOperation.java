 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattern.operations;

import com.Model.ProcessManager;
import com.bean.ImageRatioBean;
import com.db.operations.ImageRatioOperation;
import com.db.operations.MasterImageRationOperation;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;  
import java.util.ArrayList;

/**
 *
 * @author Sagar
 */
public class QuesPaperImageRatioOperation {

    public void addQuesPaperRatio() {
        String src = "masterimages/";
        String dest = "images/";
        String processPath = new ProcessManager().getProcessPath().trim() + "/images/";
        File srcFile = new File(src);
        File[] listOfFiles = srcFile.listFiles();
        for(File file : listOfFiles) {
            if (file.isFile() && (file.getName().contains("JEE") || file.getName().contains("NEET"))) {
                copyFile(src+file.getName(), dest+file.getName());
                copyFile(src+file.getName(), processPath+file.getName());
                
            }
        }
        ArrayList<ImageRatioBean> ImageRatioList = new MasterImageRationOperation().getImageRatioList();
        ArrayList<ImageRatioBean> selectedImageRatioList = new  ArrayList<ImageRatioBean>();
        
        String imageName;
        for(ImageRatioBean imageRatioBean : ImageRatioList) {
            imageName = imageRatioBean.getImageName().trim();
            if(imageName.contains("JEE") || imageName.contains("NEET"))  {
                imageRatioBean.setImageName("i"+imageName.substring(7, imageName.length()));
                selectedImageRatioList.add(imageRatioBean);
            }
        }
        
        if(selectedImageRatioList.size() != 0) {
            new ImageRatioOperation().insertImageRatioList(selectedImageRatioList);
        }
    }

    private void copyFile(String sourcePath, String destPath) {
        File sourceFile = new File(sourcePath);
        File destFile = new File(destPath);
        FileChannel source = null;
        FileChannel destination = null;
        try {
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            source = new RandomAccessFile(sourceFile, "rw").getChannel();
            destination = new RandomAccessFile(destFile, "rw").getChannel();

            long position = 0;
            long count = source.size();

            source.transferTo(position, count, destination);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
