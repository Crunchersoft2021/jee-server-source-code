/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ChangeIdReferenceBean;
import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import com.bean.MasterSubjectBean;
import com.bean.QuestionBean;
import com.bean.SubMasterChapterBean;
import com.bean.SubMasterChapterIdReferenceBean;
import com.db.DbConnection;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class MasterQuestionOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<QuestionBean> getQuestionList(ArrayList<MasterSubjectBean> selectedMasterSubjectList) {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM MASTER_QUESTION_INFO WHERE SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            for(MasterSubjectBean msb : selectedMasterSubjectList) {
                ps.setInt(1, msb.getSubjectId());
                rs = ps.executeQuery();
                
                while (rs.next()) {
                    bean = new QuestionBean();
                    bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                    bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                    bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                    bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                    bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                    bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                    bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                    bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                    bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                    bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                    bean.setYear(rs.getString(21));                  bean.setSelected(false);
                    bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));
                    if(returnList == null)
                        returnList = new ArrayList<QuestionBean>();

                    returnList.add(bean);
                }
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<QuestionBean> getChapterWiseQuestionList(ArrayList<SubMasterChapterIdReferenceBean> changeChapterIdList) {
        ArrayList<QuestionBean> returnList = null;
        ArrayList<Integer> chapterIdList = getChapterList(changeChapterIdList);
        QuestionBean bean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM MASTER_QUESTION_INFO WHERE CHAPTER_ID = ?";
            ps = conn.prepareStatement(query);
            if(chapterIdList != null) {
                for(int chapterId : chapterIdList) {
                    ps.setInt(1, chapterId);
                    rs = ps.executeQuery();
                
                    while (rs.next()) {
                        bean = new QuestionBean();
                        bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                        bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                        bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                        bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                        bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                        bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                        bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                        bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                        bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                        bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                        bean.setYear(rs.getString(21));                  bean.setSelected(false);
                        bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));
                        if(returnList == null)
                          returnList = new ArrayList<QuestionBean>();

                        returnList.add(bean);
                    }
                }
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<ImageRatioBean> insertQuestionList(ArrayList<QuestionBean> questionList,String fileLocation,ArrayList<ChangeIdReferenceBean> referenceList,ArrayList<ImageRatioBean> selectedImageRatioList) {
        int chapterId = 0;
        String queImgPath = "";
        String optImgPath = "";
        String hintImgPath = "";
        double viewDimention = 0.0;
        ImageRatioBean imageRatioBean = null;
        ArrayList<ImageRatioBean> returnList = null;
//        questionId = new NewIdOperation().getNewId("MASTER_QUESTION_INFO");
        conn = new DbConnection().getConnection();
        try{
         
           String query = "INSERT INTO MASTER_QUESTION_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

         
            ps=conn.prepareStatement(query);
            for(QuestionBean questionBean : questionList) {
                for(ChangeIdReferenceBean chapterReferenceBean : referenceList) {
                    if(questionBean.getChapterId() == chapterReferenceBean.getOldId()) {
                        chapterId = chapterReferenceBean.getNewId();
                    }
                }
//                ps.setInt(1, questionId);
                ps.setInt(1, questionBean.getQuestionId());
                ps.setString(2, questionBean.getQuestion());
                ps.setString(3, questionBean.getOptionA());
                ps.setString(4, questionBean.getOptionB());
                ps.setString(5, questionBean.getOptionC());
                ps.setString(6, questionBean.getOptionD());
                ps.setString(7, questionBean.getAnswer());
                ps.setString(8, questionBean.getHint());
                ps.setInt(9, questionBean.getLevel());
                ps.setInt(10, questionBean.getSubjectId());
                ps.setInt(11, chapterId);
                ps.setInt(12, chapterId);
                
                if(questionBean.isIsQuestionAsImage()) {
                    queImgPath = "masterImages/"+chapterId+"Question"+questionBean.getQuestionId()+".png";        
//                    queImgPath = "images/"+chapterId+"Question"+questionId+".png";
                    ps.setBoolean(13, true);
                    copyFile(fileLocation+"/"+questionBean.getQuestionImagePath(), queImgPath);
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getQuestionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(queImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(returnList == null)
                            returnList = new ArrayList<ImageRatioBean>();
                        returnList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(13, false);
                    queImgPath = "";
                }
                ps.setString(14, queImgPath);
        
                if(questionBean.isIsOptionAsImage()) {
                    optImgPath = "masterImages/"+chapterId+"Option"+questionBean.getQuestionId()+".png";
//                    optImgPath = "images/"+chapterId+"Option"+questionId+".png";
                    ps.setBoolean(15, true);
                    copyFile(fileLocation+"/"+questionBean.getOptionImagePath(), optImgPath);
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getOptionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(optImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(returnList == null)
                            returnList = new ArrayList<ImageRatioBean>();
                        returnList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(15, false);
                    optImgPath = "";
                }
//                ps.setBoolean(15, questionBean.isIsOptionAsImage());
                ps.setString(16, optImgPath);
                
                if(questionBean.isIsHintAsImage()) {
                    hintImgPath = "masterImages/"+chapterId+"Hint"+questionBean.getQuestionId()+".png";
//                    hintImgPath = "images/"+chapterId+"Hint"+questionId+".png";
                    ps.setBoolean(17, true);
                    copyFile(fileLocation+"/"+questionBean.getHintImagePath(), hintImgPath);
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getHintImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(hintImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(returnList == null)
                            returnList = new ArrayList<ImageRatioBean>();
                        returnList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(17, false);
                    hintImgPath = "";
                }
//                ps.setBoolean(17, questionBean.isIsHintAsImage());
                ps.setString(18, hintImgPath);
                ps.setInt(19, questionBean.getAttempt());
                ps.setInt(20, questionBean.getType());
                ps.setString(21, questionBean.getYear());
                ps.setString(22, questionBean.getNumericalAnswer());
                ps.setString(23, questionBean.getUnit());
                ps.executeUpdate();
//                questionId++;
            }
        }
        catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public ArrayList<ImageRatioBean> insertAddQuestionBean(QuestionBean questionBean,int questionId,ArrayList<SubMasterChapterBean> subMasterChapterList,int groupId,ChapterBean chapterBean) {
        int chapterId = 0;
        String queImgPath = "";
        String optImgPath = "";
        String hintImgPath = "";
        double viewDimention = 0.0;
        ImageRatioBean imageRatioBean = null;
        ArrayList<ImageRatioBean> returnList = null;
//        int questionId = new NewIdOperation().getNewId("MASTER_QUESTION_INFO");
        conn = new DbConnection().getConnection();
        try{
            System.out.println("add question master---------------------");
            String query = "INSERT INTO MASTER_QUESTION_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
                for(SubMasterChapterBean masterChapterBean : subMasterChapterList) {
                    if(masterChapterBean.getChapterName().trim().equalsIgnoreCase(chapterBean.getChapterName().trim()) 
                        && masterChapterBean.getSubjectBean().getSubjectId() == chapterBean.getSubjectId()
                        && masterChapterBean.getGroupBean().getGroupId() == groupId) {
                        String[] chapterIds = masterChapterBean.getChapterIds().trim().split(",");
                        try {
                            chapterId = Integer.parseInt(chapterIds[chapterIds.length-1].trim());
                        } catch(Exception ex) {
                            chapterId = 0;
                        }
                    }
                }
                
                ps.setInt(1, questionId);
                ps.setString(2, questionBean.getQuestion());
                ps.setString(3, questionBean.getOptionA());
                ps.setString(4, questionBean.getOptionB());
                ps.setString(5, questionBean.getOptionC());
                ps.setString(6, questionBean.getOptionD());
                ps.setString(7, questionBean.getAnswer());
                ps.setString(8, questionBean.getHint());
                ps.setInt(9, questionBean.getLevel());
                ps.setInt(10, questionBean.getSubjectId());
                ps.setInt(11, chapterId);
                ps.setInt(12, chapterId);
                
                if(questionBean.isIsQuestionAsImage()) {
                    queImgPath = "masterImages/"+chapterId+"Question"+questionId+".png";        
//                    queImgPath = "images/"+chapterId+"Question"+questionId+".png";
                    ps.setBoolean(13, true);
                    copyFile(questionBean.getQuestionImagePath(), queImgPath);
                    
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(queImgPath);
                    imageRatioBean.setViewDimention(viewDimention);
                    if(returnList == null)
                        returnList = new ArrayList<ImageRatioBean>();
                    returnList.add(imageRatioBean);
                } else {
                    ps.setBoolean(13, false);
                    queImgPath = "";
                }
                ps.setString(14, queImgPath);
        
                if(questionBean.isIsOptionAsImage()) {
                    optImgPath = "masterImages/"+chapterId+"Option"+questionId+".png";
//                    optImgPath = "images/"+chapterId+"Option"+questionId+".png";
                    ps.setBoolean(15, true);
                    copyFile(questionBean.getOptionImagePath(), optImgPath);
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(optImgPath);
                    imageRatioBean.setViewDimention(viewDimention);
                    if(returnList == null)
                        returnList = new ArrayList<ImageRatioBean>();
                    returnList.add(imageRatioBean);
                } else {
                    ps.setBoolean(15, false);
                    optImgPath = "";
                }
//                ps.setBoolean(15, questionBean.isIsOptionAsImage());
                ps.setString(16, optImgPath);
                
                if(questionBean.isIsHintAsImage()) {
                    hintImgPath = "masterImages/"+chapterId+"Hint"+questionId+".png";
//                    hintImgPath = "images/"+chapterId+"Hint"+questionId+".png";
                    ps.setBoolean(17, true);
                    copyFile(questionBean.getHintImagePath(), hintImgPath);
                    imageRatioBean = new ImageRatioBean();
                    imageRatioBean.setImageName(hintImgPath);
                    imageRatioBean.setViewDimention(viewDimention);
                    if(returnList == null)
                        returnList = new ArrayList<ImageRatioBean>();
                    returnList.add(imageRatioBean);
                } else {
                    ps.setBoolean(17, false);
                    hintImgPath = "";
                }
//                ps.setBoolean(17, questionBean.isIsHintAsImage());
                ps.setString(18, hintImgPath);
                ps.setInt(19, 0);
                ps.setInt(20, questionBean.getType());
                ps.setString(21, questionBean.getYear());
                ps.setString(22, questionBean.getNumericalAnswer());
                ps.setString(23, questionBean.getUnit());
                ps.executeUpdate();
//                questionId++;
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    private void copyFile(String sourcePath, String destPath) {
        File sourceFile = new File(sourcePath);
        File destFile = new File(destPath);
        FileChannel source = null;
        FileChannel destination = null;
        try {
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            source = new RandomAccessFile(sourceFile, "rw").getChannel();
            destination = new RandomAccessFile(destFile, "rw").getChannel();

            long position = 0;
            long count = source.size();

            source.transferTo(position, count, destination);
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private double getViewDimention(ArrayList<ImageRatioBean> imageRatioList,String path) {
        double returnValue = 0.0;
        for(ImageRatioBean imageRatioBean : imageRatioList) {
            if(imageRatioBean.getImageName().trim().equalsIgnoreCase(path)) {
                returnValue = imageRatioBean.getViewDimention();
                break;
            }
        }
        return returnValue;
    }
    
    private ArrayList<Integer> getChapterList(ArrayList<SubMasterChapterIdReferenceBean> changeChapterIdList) {
        ArrayList<Integer> returnList = null;
        for(SubMasterChapterIdReferenceBean bean : changeChapterIdList) {
            String[] strId = bean.getMasterChapterIds().split(",");
            int[] intId = new int[strId.length];
            for(String chpId : strId) {
                if(returnList == null)
                    returnList = new ArrayList<Integer>();
                int chapterId = Integer.parseInt(chpId);
                if(!returnList.contains(chapterId)) {
                    returnList.add(chapterId);
                }
            }
        }
        return returnList;
    }
    
    public ArrayList<QuestionBean> getQuestionList(int subjectId) {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM MASTER_QUESTION_INFO WHERE SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();

                returnList.add(bean);
            }
            
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public QuestionBean getMasterQuestionId(QuestionBean questionBean) {
        QuestionBean returnBean = null;
        ArrayList<QuestionBean> questionList = getQuestionList(questionBean.getSubjectId());
        
        if(questionList != null) {
            for(QuestionBean bean : questionList) {
                if(questionBean.getQuestion().trim().equalsIgnoreCase(bean.getQuestion().trim()) && 
                    questionBean.getOptionA().trim().equalsIgnoreCase(bean.getOptionA().trim()) &&
                    questionBean.getOptionB().trim().equalsIgnoreCase(bean.getOptionB().trim()) &&
                    questionBean.getOptionC().trim().equalsIgnoreCase(bean.getOptionC().trim()) &&
                    questionBean.getOptionD().trim().equalsIgnoreCase(bean.getOptionD().trim()) &&
                    questionBean.getHint().trim().equalsIgnoreCase(bean.getHint().trim()) &&
                    questionBean.isIsQuestionAsImage() == bean.isIsQuestionAsImage() &&
                    questionBean.isIsOptionAsImage() == bean.isIsOptionAsImage() &&
                    questionBean.isIsHintAsImage() == bean.isIsHintAsImage()) {
                    returnBean = bean;
                    break;
                }
            }
        }
        
        return returnBean;
    }
    
    public boolean updateModifyQuestion(QuestionBean questionBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        System.out.println("Modified into MASTER_QUESTION_INFO");
        try{
            String query = "UPDATE MASTER_QUESTION_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,"
                    + "ANSWER = ?,HINT = ?,QUESTION_LEVEL = ?,CHAPTER_ID = ?,TOPIC_ID = ?,ISQUESTIONASIMAGE = ?,"
                    + "QUESTIONIMAGEPATH = ?,ISOPTIONASIMAGE = ?,OPTIONIMAGEPATH = ?,ISHINTASIMAGE = ?,HINTIMAGEPATH = ?,"
                    + "QUESTION_TYPE = ?,QUESTION_YEAR = ?,NUMERICAL_ANSWER=?,UNIT=? WHERE ANSWER = ? AND OPTIONA = ? AND OPTIONB = ? AND OPTIONC = ? AND OPTIOND = ?";
            
            ps=conn.prepareStatement(query);
            
            ps.setString(1, questionBean.getQuestion());
            ps.setString(2, questionBean.getOptionA());
            ps.setString(3, questionBean.getOptionB());
            ps.setString(4, questionBean.getOptionC());
            ps.setString(5, questionBean.getOptionD());
            ps.setString(6, questionBean.getAnswer());
            ps.setString(7, questionBean.getHint());
            ps.setInt(8, questionBean.getLevel());
            ps.setInt(9, questionBean.getChapterId());
            ps.setInt(10, questionBean.getTopicId());
            ps.setBoolean(11, questionBean.isIsQuestionAsImage());
            ps.setString(12, questionBean.getQuestionImagePath());
            ps.setBoolean(13, questionBean.isIsOptionAsImage());
            ps.setString(14, questionBean.getOptionImagePath());
            ps.setBoolean(15, questionBean.isIsHintAsImage());
            ps.setString(16, questionBean.getHintImagePath());
            ps.setInt(17, questionBean.getType());
            ps.setString(18, questionBean.getYear());
            ps.setString(19,questionBean.getNumericalAnswer());
            System.out.println("masterquestionBean update query----"+questionBean.getNumericalAnswer());
            ps.setString(20,questionBean.getUnit());
            //ps.setInt(21, questionBean.getQuestionId());
            ps.setString(21, questionBean.getAnswer());
            ps.setString(22, questionBean.getOptionA());
            ps.setString(23, questionBean.getOptionB());
            ps.setString(24, questionBean.getOptionC());
            ps.setString(25, questionBean.getOptionD());
            System.out.println("Question------------"+questionBean.getQuestion());
            System.out.println("master Update count-------------"+ps.getUpdateCount());
            ps.executeUpdate();
            System.out.println("Modified into MASTER_QUESTION_INFO");
            System.out.println("MASTER_QUESTION_TYPE-----------"+questionBean.getType());
            returnValue = true;
        }
        catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public ArrayList<QuestionBean> getQuestuionsList() {
        ArrayList<QuestionBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM MASTER_QUESTION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            QuestionBean bean = null;
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                 bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                 bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                 bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                  bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                      bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                 bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));     bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));       bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));         bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                   bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                   bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));        bean.setUnit(rs.getString(23));
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();
                
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public void updateRemoveUnwanted(ArrayList<QuestionBean> questionList) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE MASTER_QUESTION_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,HINT = ?,QUESTION_YEAR = ?  WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, questionBean.getQuestion().trim());
                ps.setString(2, questionBean.getOptionA().trim());
                ps.setString(3, questionBean.getOptionB().trim());
                ps.setString(4, questionBean.getOptionC().trim());
                ps.setString(5, questionBean.getOptionD().trim());
                ps.setString(6, questionBean.getHint().trim());
                ps.setString(7, questionBean.getYear().trim());
                ps.setInt(8, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public boolean deleteQuestion (QuestionBean deleteQuestionBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM MASTER_OPTION_IMAGE_DIMENSIONS WHERE QUEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, deleteQuestionBean.getQuestionId());
            ps.execute();
            
            query = "DELETE FROM MASTER_QUESTION_INFO WHERE QUESTION_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, deleteQuestionBean.getQuestionId());
            returnValue = ps.execute();
            
            ArrayList<String> imageList = null;
            
            if(deleteQuestionBean.isIsQuestionAsImage() || deleteQuestionBean.isIsHintAsImage() || deleteQuestionBean.isIsOptionAsImage()) {
                imageList = new ArrayList<String>();
                if(deleteQuestionBean.isIsQuestionAsImage())
                    imageList.add(deleteQuestionBean.getQuestionImagePath());
                
                if(deleteQuestionBean.isIsOptionAsImage())
                    imageList.add(deleteQuestionBean.getOptionImagePath());
                
                if(deleteQuestionBean.isIsHintAsImage())
                    imageList.add(deleteQuestionBean.getHintImagePath());
                
                new MasterImageRationOperation().deleteImageNameList(imageList);
            }
            
            if(deleteQuestionBean.isIsQuestionAsImage()) {
                deleteImageFile(deleteQuestionBean.getQuestionImagePath());
            }
            
            if(deleteQuestionBean.isIsOptionAsImage()) {
                deleteImageFile(deleteQuestionBean.getOptionImagePath());
            }

            if(deleteQuestionBean.isIsHintAsImage()) {
                deleteImageFile(deleteQuestionBean.getHintImagePath());
            }
         } catch (SQLException ex) {
            returnValue=true;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    private void deleteImageFile(String imageName) {
        File f1 = new File(imageName);
        f1.delete();
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
