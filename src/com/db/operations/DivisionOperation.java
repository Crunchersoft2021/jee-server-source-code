/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.DivisionBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aniket
 */
public class DivisionOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public int getNewId(){
        int newId = 0;
        try {
            conn = new DbConnection().getConnection();
            stmt = conn.createStatement();
            String query = "SELECT COUNT(*) FROM DIVISION_INFO ";
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(stmt != null)
                    stmt.close();
                if(conn != null)
                    conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return newId;
    }
    public ArrayList<DivisionBean> getDivisionList() {
        ArrayList<DivisionBean> returnList = null;
        DivisionBean divisionBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM DIVISION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next()) {
                divisionBean = new DivisionBean();
                divisionBean.setDivisionId(rs.getInt(1));
                divisionBean.setName(rs.getString(2));
                
                if(returnList == null)
                    returnList = new ArrayList<DivisionBean>();

                returnList.add(divisionBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public boolean insertDivisionBean(DivisionBean divisionBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO DIVISION_INFO VALUES(?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, divisionBean.getDivisionId());
            ps.setString(2, divisionBean.getName());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean updateDivisionBean(DivisionBean divisionBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE DIVISION_INFO SET DIVISION = ? WHERE DIV_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setString(1, divisionBean.getName());
            ps.setInt(2, divisionBean.getDivisionId());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnValue;
    }
    
    public boolean insertDivisionList(List<DivisionBean> divisionList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO DIVISION_INFO VALUES(?,?)";
            for(DivisionBean divisionBean : divisionList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, divisionBean.getDivisionId());
                ps.setString(2, divisionBean.getName());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean updateDivisionList(List<DivisionBean> divisionList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE DIVISION_INFO SET DIVISION = ? WHERE DIV_ID = ?";
            for(DivisionBean divisionBean : divisionList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, divisionBean.getName());
                ps.setInt(2, divisionBean.getDivisionId());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
