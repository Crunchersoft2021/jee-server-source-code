/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class ImageRatioOperation {
    private Connection conn;
    private ResultSet rs;
    private PreparedStatement ps;
    
    private ImageRatioBean imageRatioBean;
    private ArrayList<ImageRatioBean> returnList;

    public ArrayList<ImageRatioBean> getImageRatioList() {
        ArrayList<ImageRatioBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM IMAGERATIO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            ImageRatioBean imageRatioBean = null;
            
            while(rs.next()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(rs.getString(1));
                imageRatioBean.setViewDimention(rs.getDouble(2));
                if(returnList == null)
                    returnList = new ArrayList<ImageRatioBean>();
                returnList.add(imageRatioBean);
            }
        } catch(Exception ex) {
            returnList = null;
            ex.getMessage();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
     public ArrayList<ImageRatioBean> getImageRatioList(Connection conn) {
        ArrayList<ImageRatioBean> returnList = null;
        try {
            ImageRatioBean imageRatioBean = null;
            String query = "SELECT * FROM IMAGERATIO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(rs.getString(1));
                imageRatioBean.setViewDimention(rs.getDouble(2));
                
                if(returnList == null)
                    returnList = new ArrayList<ImageRatioBean>();
                
                returnList.add(imageRatioBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        return returnList;
    }
    public ArrayList<ImageRatioBean> getImageRatioList(ArrayList<QuestionBean> selectedQuestionsList){
        returnList = new ArrayList<ImageRatioBean>();
        try{
            conn = new DbConnection().getConnection();
            String query = "select * from ImageRatio where ImageName=?";
            ps=conn.prepareStatement(query);
           
            for(QuestionBean questionsBean : selectedQuestionsList){
                if(questionsBean.isIsQuestionAsImage())
                    setImageRatioList(questionsBean.getQuestionImagePath());
                if(questionsBean.isIsOptionAsImage())
                    setImageRatioList(questionsBean.getOptionImagePath());
                if(questionsBean.isIsHintAsImage())
                    setImageRatioList(questionsBean.getHintImagePath());
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    private void setImageRatioList(String path){
        try {
            ps.setString(1,path);
            rs=ps.executeQuery();
            while(rs.next())
            {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(rs.getString(1));
                imageRatioBean.setViewDimention(rs.getDouble(2));
                returnList.add(imageRatioBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean insertImageRatioList(ArrayList<ImageRatioBean> imageRatioList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "INSERT INTO IMAGERATIO VALUES(?,?)";
            for(ImageRatioBean imageRatioBean : imageRatioList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, imageRatioBean.getImageName());
                ps.setDouble(2, imageRatioBean.getViewDimention());
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean updateModifyQuesImageRatio(ArrayList<String> imageRatioList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "";
            for(String str : imageRatioList) {
                String[] strArray = str.split("##");
                if(strArray.length == 3) {
                    query = "UPDATE MASTER_IMAGERATIO SET IMAGENAME = ?,VIEWDIME = ? WHERE IMAGENAME = ?";
                    ps = conn.prepareStatement(query);
                    ps.setString(1, strArray[1].trim());
                    ps.setDouble(2, Double.parseDouble(strArray[2].trim()));
                    ps.setString(3, strArray[0].trim());
                    ps.executeUpdate();
                } else if(strArray.length == 2) {
                    query = "INSERT INTO IMAGERATIO VALUES(?,?)";
                    ps = conn.prepareStatement(query);
                    ps.setString(1, strArray[0].trim());
                    ps.setDouble(2, Double.parseDouble(strArray[1].trim()));
                    ps.executeUpdate();
                } else if(strArray.length == 1) {
                    query = "DELETE FROM IMAGERATIO WHERE IMAGENAME = ?";
                    ps = conn.prepareStatement(query);
                    ps.setString(1, strArray[0].trim());
                    ps.execute();
                }
                
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public boolean deleteImageNameList(ArrayList<String> imageNameList) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM IMAGERATIO WHERE IMAGENAME = ?";
            for(String imageName : imageNameList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, imageName);
                ps.executeUpdate();
            }
            returnValue = true;
        } catch (SQLException ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
//    //Pattern Process
//    public boolean insertImageRatioList(ArrayList<ImageRatioBean> imageRationList) {
//        boolean returnValue = false;
//        try {
//            conn = new DbConnection().getConnection();
//            String query = "INSERT INTO IMAGERATIO VALUES(?,?)";
//            ps = conn.prepareStatement(query);
//            for(ImageRatioBean imageRatioBean : imageRationList) {
//                ps.setString(1, imageRatioBean.getImageName());
//                ps.setDouble(2, imageRatioBean.getViewDimention());
//                ps.executeUpdate();
//            }
//            returnValue = true;
//        } catch (SQLException ex) {
//            returnValue = false;
//            ex.printStackTrace();
//        } finally {
//            sqlClose();
//        }
//        return returnValue;
//    }
    
    
    //    for Import File Operation.............
    
    public ArrayList<ImageRatioBean> getMasterImageRatioList(Connection conn) {
        ArrayList<ImageRatioBean> returnList = null;
        try {
            ImageRatioBean imageRatioBean = null;
            String query = "SELECT * FROM MASTER_IMAGERATIO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
                imageRatioBean = new ImageRatioBean();
                imageRatioBean.setImageName(rs.getString(1));
                imageRatioBean.setViewDimention(rs.getDouble(2));
                
                if(returnList == null)
                    returnList = new ArrayList<ImageRatioBean>();
                
                returnList.add(imageRatioBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }    
        return returnList;
    }
    
    
    private void sqlClose() {
        try {
            if(rs!=null)
                rs.close();
            if(ps!=null)
                ps.close();
            if(conn!=null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
