/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.ChapterBean;
import com.bean.TestingSettingBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class TestingSettingOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    
     public TestingSettingBean getTestingSettingBean(Boolean Status) {
       TestingSettingBean returnBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM TESTING_SETTING_INFO WHERE Status = ?";
            ps = conn.prepareStatement(query);
            ps.setBoolean(1, Status);
            rs = ps.executeQuery();
            while(rs.next()) {
                returnBean = new TestingSettingBean();
                returnBean.setTID(rs.getInt(1));
                returnBean.setTestingType(rs.getString(2));
                returnBean.setStatus(rs.getBoolean(3));   
                
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
     
     public boolean updateStatustesting(int TID) {
        boolean returnValue = false;
        try {
            updateStatustesting();
            conn = new DbConnection().getConnection();
            String query = "UPDATE TESTING_SETTING_INFO SET Status = ? WHERE TID = ?";
            ps = conn.prepareStatement(query);
           
                ps.setBoolean(1, true);
                ps.setInt(2, TID );
                ps.executeUpdate();
          
            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    } 
     
    public boolean updateStatustesting() throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE TESTING_SETTING_INFO SET Status=? WHERE Status ";
            ps = conn.prepareStatement(query);            
            ps.setBoolean(1,false);
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
   private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }  
}
