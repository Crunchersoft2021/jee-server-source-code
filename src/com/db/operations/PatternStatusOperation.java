/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.Model.SwapData;
import com.bean.RegistrationBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aniket
 */
public class PatternStatusOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
     public void updatePatternStatus(int PID) {
       
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE PATTERN_STATUS_INFO SET STATUS = ? WHERE PID=?";
            ps = conn.prepareStatement(query);            
            ps.setBoolean(1, true);   
            ps.setInt(2, PID);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();            
        } finally {
            sqlClose();
        }
        
    }
     
     public boolean updateAllPatternStatus() throws SQLException {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE PATTERN_STATUS_INFO SET STATUS = ? WHERE STATUS ";
            ps = conn.prepareStatement(query);            
            ps.setBoolean(1,false);
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            conn.close();
        }
        return returnValue;
    }
     
     public String getCurrentPatternName()
     {
        String CurrentPatternName=null; 
        try {
           
            conn=new DbConnection().getConnection();
            String query="SELECT PATTER_NAME FROM PATTERN_STATUS_INFO WHERE STATUS=?";
            ps=conn.prepareStatement(query);
            ps.setBoolean(1, true);
            rs=ps.executeQuery();
            while(rs.next())
            {
                CurrentPatternName=rs.getString(1);
             
                return CurrentPatternName;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PatternStatusOperation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CurrentPatternName;
     }
     
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    } 
}
