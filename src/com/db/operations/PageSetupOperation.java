/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.HeaderFooterTextBean;
import com.bean.PdfPageSetupBean;
import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aniket
 */
public class PageSetupOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public PdfPageSetupBean getPdfPagesSetup(){
        PdfPageSetupBean returnBean = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM PAGE_SETUP_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
                returnBean = new PdfPageSetupBean();
                returnBean.setPageFormat(rs.getInt(1));
                returnBean.setTwoColumn(rs.getBoolean(2));
                returnBean.setPrintYear(rs.getBoolean(3));
                returnBean.setPaperType(rs.getString(4));
                
                returnBean.setSetLogo(rs.getBoolean(5));
                returnBean.setLogoPath(rs.getString(6));
                returnBean.setSetImageActualsize(rs.getBoolean(7));
                returnBean.setLogoPosition(rs.getInt(8));
                
                returnBean.setWaterMarkInfo(rs.getInt(9));
                returnBean.setWaterMarkTextGrayScale(rs.getInt(10));
                returnBean.setWaterMarkScale(rs.getInt(11));
                returnBean.setWaterMarkAngle(rs.getInt(12));
                
                returnBean.setWaterMarkLogoPath(rs.getString(13));
                HeaderFooterTextBean headerFooterTextBean = null;
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(14));
                headerFooterTextBean.setTextValue(rs.getString(15));
                returnBean.setHeaderLeftBean(headerFooterTextBean);
                
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(16));
                headerFooterTextBean.setTextValue(rs.getString(17));
                returnBean.setHeaderCenterBean(headerFooterTextBean);
                
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(18));
                headerFooterTextBean.setTextValue(rs.getString(19));
                returnBean.setHeaderRightBean(headerFooterTextBean);
                
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(20));
                headerFooterTextBean.setTextValue(rs.getString(21));
                returnBean.setFooterLeftBean(headerFooterTextBean);
                
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(22));
                headerFooterTextBean.setTextValue(rs.getString(23));
                returnBean.setFooterCenterBean(headerFooterTextBean);
                
                headerFooterTextBean = new HeaderFooterTextBean();
                headerFooterTextBean.setCheckBoxSelected(rs.getBoolean(24));
                headerFooterTextBean.setTextValue(rs.getString(25));
                returnBean.setFooterRightBean(headerFooterTextBean);
                
                returnBean.setHeaderFooterOnFirstPage(rs.getBoolean(26));
                returnBean.setQuestionStartNo(rs.getInt(27));
                returnBean.setQuestionBold(rs.getBoolean(28));
                returnBean.setOptionBold(rs.getBoolean(29));
                returnBean.setSolutionBold(rs.getBoolean(30));
                returnBean.setChapterList(rs.getBoolean(31));
                returnBean.setPageBorder(rs.getBoolean(32));
                returnBean.setBorderWidth(rs.getInt(33));
                returnBean.setOptimizeLineSpace(rs.getBoolean(34));
                returnBean.setFontSize(rs.getInt(35));
            }
//            return returnBean;
        } catch(Exception ex) {
            returnBean = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnBean;
    }
    
    public boolean insertPdfPagesSetup(PdfPageSetupBean pageSetupBean) {
        boolean returnValue = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM PAGE_SETUP_INFO"; 
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
            
            query = "INSERT INTO PAGE_SETUP_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            
            ps.setInt(1, pageSetupBean.getPageFormat());
            ps.setBoolean(2, pageSetupBean.isTwoColumn());
            ps.setBoolean(3, pageSetupBean.isPrintYear());
            ps.setString(4, pageSetupBean.getPaperType());
            ps.setBoolean(5, pageSetupBean.isSetLogo());
            ps.setString(6, pageSetupBean.getLogoPath());
            ps.setBoolean(7, pageSetupBean.isSetImageActualsize());
            ps.setInt(8, pageSetupBean.getLogoPosition());
            ps.setInt(9, pageSetupBean.getWaterMarkInfo());
            ps.setInt(10, pageSetupBean.getWaterMarkTextGrayScale());
            ps.setInt(11, pageSetupBean.getWaterMarkScale());
            ps.setInt(12, pageSetupBean.getWaterMarkAngle());
            ps.setString(13, pageSetupBean.getWaterMarkLogoPath());
            ps.setBoolean(14, pageSetupBean.getHeaderLeftBean().isCheckBoxSelected());
            ps.setString(15, pageSetupBean.getHeaderLeftBean().getTextValue().trim());
            ps.setBoolean(16, pageSetupBean.getHeaderCenterBean().isCheckBoxSelected());
            ps.setString(17, pageSetupBean.getHeaderCenterBean().getTextValue().trim());
            ps.setBoolean(18, pageSetupBean.getHeaderRightBean().isCheckBoxSelected());
            ps.setString(19, pageSetupBean.getHeaderRightBean().getTextValue().trim());
            ps.setBoolean(20, pageSetupBean.getFooterLeftBean().isCheckBoxSelected());
            ps.setString(21, pageSetupBean.getFooterLeftBean().getTextValue().trim());
            ps.setBoolean(22, pageSetupBean.getFooterCenterBean().isCheckBoxSelected());
            ps.setString(23, pageSetupBean.getFooterCenterBean().getTextValue().trim());
            ps.setBoolean(24, pageSetupBean.getFooterRightBean().isCheckBoxSelected());
            ps.setString(25, pageSetupBean.getFooterRightBean().getTextValue().trim());
            ps.setBoolean(26, pageSetupBean.isHeaderFooterOnFirstPage());
            ps.setInt(27, pageSetupBean.getQuestionStartNo());
            ps.setBoolean(28, pageSetupBean.isQuestionBold());
            ps.setBoolean(29, pageSetupBean.isOptionBold());
            ps.setBoolean(30, pageSetupBean.isSolutionBold());
            ps.setBoolean(31, pageSetupBean.isChapterList());
            ps.setBoolean(32, pageSetupBean.isPageBorder());
            ps.setInt(33, pageSetupBean.getBorderWidth());
            ps.setBoolean(34, pageSetupBean.isOptimizeLineSpace());
            ps.setInt(35,pageSetupBean.getFontSize());
            ps.executeUpdate();

            returnValue = true;
        } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public void deletePageSetupInfo() {
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM PAGE_SETUP_INFO"; 
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
