/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pages;

import com.Model.TitleInfo;
import com.bean.ImageRatioBean;
import com.bean.ImportChapterBean;
import com.bean.ImportOptionImageDimensionsBean;
import com.bean.NewAndOldBean;
import com.bean.QuestionBean;
import com.db.DbConnectionForImport;
import com.db.operations.ChapterOperation;
import com.db.operations.ImageRatioOperation;
import com.db.operations.NewAndOldOperations;
import com.db.operations.NewIdOperation;
import com.db.operations.OptionImageDimentionOperation;
import com.db.operations.QuestionOperation;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class ImportDataProcess extends javax.swing.JFrame {
    
    
    private Connection srcConnection;
    private Connection dstConnection;
    private ArrayList<ImportChapterBean> srcChapterList;
    private ArrayList<ImportChapterBean> destChapterList;
    private ArrayList<ImportChapterBean> sortedSrcChapterList;
    private ArrayList<ImportChapterBean> sortedDstChapterList;
    private ArrayList<QuestionBean> srcQuestionList;
    private ArrayList<QuestionBean> sortedSrcQuestionList;
    private ArrayList<ImageRatioBean> srcImageRatioList;
    private ArrayList<ImportOptionImageDimensionsBean> srcOptionDimList;
    private ArrayList<NewAndOldBean> srcNewAndOldList;
    
    private ArrayList<ImportChapterBean> mastersrcChapterList;
    private ArrayList<ImportChapterBean> masterdestChapterList;
    private ArrayList<ImportChapterBean> mastersortedSrcChapterList;
    private ArrayList<ImportChapterBean> mastersortedDstChapterList;
    private ArrayList<QuestionBean> mastersrcQuestionList;
    private ArrayList<QuestionBean> mastersortedSrcQuestionList;
    private ArrayList<ImageRatioBean> mastersrcImageRatioList;
    private ArrayList<ImportOptionImageDimensionsBean> mastersrcOptionDimList;
    private ArrayList<NewAndOldBean> mastersrcNewAndOldList;
    private String srcFileLocation;
    private ResultSet rs;
    private PreparedStatement ps;

    /**
     * Creates new form ImportDataProcess
     */
    public ImportDataProcess() {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
    }
    
    public ImportDataProcess(String MergePathFrom ,String ImagesPathFrom) {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        srcConnection = new DbConnectionForImport().getSorceConnection(MergePathFrom); //from
        dstConnection = new DbConnectionForImport().getDestConnection();  //to
        
        sortedSrcChapterList = new ArrayList<ImportChapterBean>();
        sortedDstChapterList = new ArrayList<ImportChapterBean>();
        sortedSrcQuestionList = new ArrayList<QuestionBean>();
                
        mastersortedSrcChapterList = new ArrayList<ImportChapterBean>();
        mastersortedDstChapterList = new ArrayList<ImportChapterBean>();
        mastersortedSrcQuestionList = new ArrayList<QuestionBean>();
                
//        srcFileLocation = "C:/Users/Aniket/Desktop/Board DB Backup/PCB";
    
        
//        srcFileLocation = "C:/Users/Aniket/Desktop/PCB/src/updated";
//       srcFileLocation = "E:\\Aniket\\D Drive\\CrunchMerge\\DATATRANSFERUTILITYMHCETMERGE\\newim";
        srcLoad();
        srcLoadMaster();
        srcFileLocation = ""+ImagesPathFrom+"";
    }

    private void srcLoad() {
        ChapterOperation cp = new ChapterOperation();
        srcChapterList = cp.getChapterList(srcConnection);
//        destChapterList1 = cp.getMasterChapterList(dstConnection);
        destChapterList = cp.getChapterList(dstConnection);//akshay
//        destChapterList = cp.getMasterChapterList(dstConnection);
        srcQuestionList = new QuestionOperation().getQuestionList(srcConnection);
        srcImageRatioList = new ImageRatioOperation().getImageRatioList(srcConnection);
//        srcOptionDimList = new OptionImageDimentionsOperations().getMasterDimentionList(srcConnection);
        srcOptionDimList = new OptionImageDimentionOperation().getDimentionList(srcConnection);
//        srcNewAndOldList = new NewAndOldOperations().getNewAndOldList(srcConnection);
    }
    private void srcLoadMaster() {
        ChapterOperation cp = new ChapterOperation();
        
        mastersrcChapterList = cp.getMasterChapterList(srcConnection);       
        masterdestChapterList = cp.getMasterChapterList(dstConnection);
        mastersrcQuestionList = new QuestionOperation().getMasterQuestionList(srcConnection);
        mastersrcImageRatioList = new ImageRatioOperation().getMasterImageRatioList(srcConnection);
        mastersrcOptionDimList = new OptionImageDimentionOperation().getMasterDimentionList(srcConnection);
       
//        srcNewAndOldList = new NewAndOldOperations().getNewAndOldList(srcConnection);
    }
    private void loadalldata1(int subId) {

        cmbFromChapter.removeAllItems();
//        chapterIdscet = new DBCONNECTIONForJEEStu().getAllChapterIds1(subId);
//        chapterlistcet = new DBCONNECTIONForJEEStu().getChapterAllName1(subId);
        
        sortedSrcChapterList.clear();
        ImportChapterBean chapterBean = null;
        for(int i = 0;i<srcChapterList.size();i++) {
            chapterBean = srcChapterList.get(i);
            if(chapterBean.getSubjectId() == subId) {
                sortedSrcChapterList.add(chapterBean);
                cmbFromChapter.addItem((i+1) + ") " + chapterBean.getChapterName() + "("+chapterBean.getBookId()+")");
            }
        }
        
        sortedSrcQuestionList = new ArrayList<QuestionBean>();
        for(QuestionBean questionsBean : srcQuestionList) {
            if(questionsBean.getSubjectId() == subId) {
                sortedSrcQuestionList.add(questionsBean);
            }
        }
//        for (int i = 0; i < chapterIdscet.size(); i++) {
//            int newid = i + 1;
//            jcomboCETChapter.addItem(newid + ") " + chapterlistcet.get(i));
//        }

    }
    
    private void loadalldata2(int subId) {

       cmbToChapter.removeAllItems();
//        chapterIdsjee = new DBCONNECTIONForJEEStu().getAllChapterIds2(subId);
//        chapterlistjee = new DBCONNECTIONForJEEStu().getChapterAllName2(subId);
//        for (int i = 0; i < chapterIdsjee.size(); i++) {
//            int newid = i + 1;
//            jcomboJEEChapter.addItem(newid + ") " + chapterlistjee.get(i));
//        }
        
        sortedDstChapterList.clear();
        ImportChapterBean chapterBean = null;
        for(int i = 0;i<destChapterList.size();i++) {
            chapterBean = destChapterList.get(i);
            if(chapterBean.getSubjectId() == subId) {
                sortedDstChapterList.add(chapterBean);
                cmbToChapter.addItem((i+1) + ") " + chapterBean.getChapterName() + "("+chapterBean.getBookId()+")");
            }
        }
    }
    
    
//     private void Masterloadalldata1(int subId) {
//
//        cmbFromMasterChapter.removeAllItems();
////        chapterIdscet = new DBCONNECTIONForJEEStu().getAllChapterIds1(subId);
////        chapterlistcet = new DBCONNECTIONForJEEStu().getChapterAllName1(subId);
//        
//        mastersortedSrcChapterList.clear();
//        ImportChapterBean chapterBean = null;
//        for(int i = 0;i<mastersrcChapterList.size();i++) {
//            chapterBean = mastersrcChapterList.get(i);
//            if(chapterBean.getSubjectId() == subId) {
//                mastersortedSrcChapterList.add(chapterBean);
//                cmbFromMasterChapter.addItem((i+1) + ") " + chapterBean.getChapterName() + "("+chapterBean.getBookId()+")");
//            }
//        }
//        
//        mastersortedSrcQuestionList = new ArrayList<QuestionBean>();
//        for(QuestionBean questionsBean : mastersrcQuestionList) {
//            if(questionsBean.getSubjectId() == subId) {
//                mastersortedSrcQuestionList.add(questionsBean);
//            }
//        }
////        for (int i = 0; i < chapterIdscet.size(); i++) {
////            int newid = i + 1;
////            jcomboCETChapter.addItem(newid + ") " + chapterlistcet.get(i));
////        }
//
//    }
    
    private void Masterloadalldata2(int subId) {

       cmbToMasterChapter.removeAllItems();
//        chapterIdsjee = new DBCONNECTIONForJEEStu().getAllChapterIds2(subId);
//        chapterlistjee = new DBCONNECTIONForJEEStu().getChapterAllName2(subId);
//        for (int i = 0; i < chapterIdsjee.size(); i++) {
//            int newid = i + 1;
//            jcomboJEEChapter.addItem(newid + ") " + chapterlistjee.get(i));
//        }
        
        mastersortedDstChapterList.clear();
        ImportChapterBean chapterBean = null;
        for(int i = 0;i<masterdestChapterList.size();i++) {
            chapterBean = masterdestChapterList.get(i);
            if(chapterBean.getSubjectId() == subId) {
                mastersortedDstChapterList.add(chapterBean);
                cmbToMasterChapter.addItem((i+1) + ") " + chapterBean.getChapterName() + "("+chapterBean.getBookId()+")");
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MainBodyPanel = new javax.swing.JPanel();
        TitlePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        SubBodyPanel = new javax.swing.JPanel();
        lblNewData = new javax.swing.JLabel();
        lblOldData = new javax.swing.JLabel();
        cmbFromSubject = new javax.swing.JComboBox<>();
        cmbToSubject = new javax.swing.JComboBox<>();
        cmbFromChapter = new javax.swing.JComboBox<>();
        chkBoxLink = new javax.swing.JCheckBox();
        cmbToChapter = new javax.swing.JComboBox<>();
        btnFromQCount = new javax.swing.JButton();
        btnToQCount = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lblquestioncountCET = new javax.swing.JLabel();
        lblquestioncountJEE = new javax.swing.JLabel();
        cmbToMasterChapter = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        TitlePanel.setBackground(new java.awt.Color(0, 0, 0));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Import Data");

        javax.swing.GroupLayout TitlePanelLayout = new javax.swing.GroupLayout(TitlePanel);
        TitlePanel.setLayout(TitlePanelLayout);
        TitlePanelLayout.setHorizontalGroup(
            TitlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TitlePanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(217, 217, 217))
        );
        TitlePanelLayout.setVerticalGroup(
            TitlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TitlePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblNewData.setFont(new java.awt.Font("Segoe UI Semibold", 0, 14)); // NOI18N
        lblNewData.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNewData.setText("Fom New Data");

        lblOldData.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        lblOldData.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOldData.setText("To Add In Old Data");

        cmbFromSubject.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None ", "Physics", "Chemistry", "Mathmatics", "Biology" }));
        cmbFromSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbFromSubjectActionPerformed(evt);
            }
        });

        cmbToSubject.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None", "Physics", "Chemistry", "Mathmatics", "Biology" }));
        cmbToSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbToSubjectActionPerformed(evt);
            }
        });
        cmbToSubject.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                cmbToSubjectAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        cmbFromChapter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFromChapterItemStateChanged(evt);
            }
        });

        chkBoxLink.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        chkBoxLink.setText("link");

        btnFromQCount.setText("Check Question Count");
        btnFromQCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFromQCountActionPerformed(evt);
            }
        });

        btnToQCount.setText("Check Question Count");
        btnToQCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnToQCountActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jButton1.setText("Transfer");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblquestioncountCET.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        lblquestioncountCET.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        lblquestioncountJEE.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        lblquestioncountJEE.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout SubBodyPanelLayout = new javax.swing.GroupLayout(SubBodyPanel);
        SubBodyPanel.setLayout(SubBodyPanelLayout);
        SubBodyPanelLayout.setHorizontalGroup(
            SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubBodyPanelLayout.createSequentialGroup()
                .addGap(286, 286, 286)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(SubBodyPanelLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(cmbFromChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblNewData, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmbFromSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblquestioncountCET, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFromQCount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubBodyPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblOldData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbToSubject, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(SubBodyPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SubBodyPanelLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblquestioncountJEE, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnToQCount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)))
                            .addGroup(SubBodyPanelLayout.createSequentialGroup()
                                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(SubBodyPanelLayout.createSequentialGroup()
                                        .addGap(49, 49, 49)
                                        .addComponent(cmbToMasterChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(SubBodyPanelLayout.createSequentialGroup()
                                        .addComponent(chkBoxLink)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cmbToChapter, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(49, 49, 49))
        );
        SubBodyPanelLayout.setVerticalGroup(
            SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SubBodyPanelLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNewData, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOldData, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFromSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbToSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFromChapter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbToChapter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkBoxLink))
                .addGap(31, 31, 31)
                .addComponent(cmbToMasterChapter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFromQCount)
                    .addComponent(btnToQCount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(SubBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblquestioncountCET, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblquestioncountJEE, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout MainBodyPanelLayout = new javax.swing.GroupLayout(MainBodyPanel);
        MainBodyPanel.setLayout(MainBodyPanelLayout);
        MainBodyPanelLayout.setHorizontalGroup(
            MainBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TitlePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(SubBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        MainBodyPanelLayout.setVerticalGroup(
            MainBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainBodyPanelLayout.createSequentialGroup()
                .addComponent(TitlePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SubBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MainBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbFromSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbFromSubjectActionPerformed
        // TODO add your handling code here:
        int index = 0;
        index = cmbFromSubject.getSelectedIndex();
//        index += 1;
        loadalldata1(index);
    
    }//GEN-LAST:event_cmbFromSubjectActionPerformed

    private void cmbToSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbToSubjectActionPerformed
        // TODO add your handling code here:\
        int index = 0;
        index = cmbToSubject.getSelectedIndex();
        //        index += 1;
        loadalldata2(index);
        Masterloadalldata2(index);
    }//GEN-LAST:event_cmbToSubjectActionPerformed

    private void cmbToSubjectAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_cmbToSubjectAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbToSubjectAncestorAdded

    private void cmbFromChapterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFromChapterItemStateChanged
        // TODO add your handling code here:
        if (chkBoxLink.isSelected()) {
            int index = 0;
            index = cmbFromChapter.getSelectedIndex();
            cmbToChapter.setSelectedIndex(index);
        }
    }//GEN-LAST:event_cmbFromChapterItemStateChanged

    private void btnFromQCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFromQCountActionPerformed
        // TODO add your handling code here:
        loadQueCount1();
    }//GEN-LAST:event_btnFromQCountActionPerformed

    private void btnToQCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnToQCountActionPerformed
        // TODO add your handling code here:
        loadQueCount2();
    }//GEN-LAST:event_btnToQCountActionPerformed

    public void loadQueCount1() {
//        String chapterName = jcomboCETChapter.getSelectedItem().toString();
//        chapterName = chapterName.substring(chapterName.indexOf(")") + 1).trim();
//        DBCONNECTIONForJEEStu ob = new DBCONNECTIONForJEEStu();
//        int chapID = ob.getChapterID1(chapterName);
//        int questinCOunt = ob.getQuestionCount1(chapID);
//        lblquestioncountCET.setText("Cureent Que Count: " + questinCOunt);
        
        int count = 0;
        int chapterId = sortedSrcChapterList.get(cmbFromChapter.getSelectedIndex()).getChapterId();
        
        for(QuestionBean bean: sortedSrcQuestionList) {
            if(bean.getChapterId() == chapterId) {
                count++;
            }
        }
        lblquestioncountCET.setText("Cureent Que Count: " + count);
    }

    public void loadQueCount2() {
        int count = 0;
        int count1 = 0;
        try {
 //        String chapterName = jcomboJEEChapter.getSelectedItem().toString();
//        chapterName = chapterName.substring(chapterName.indexOf(")") + 1).trim();
//        DBCONNECTIONForJEEStu ob = new DBCONNECTIONForJEEStu();
//        int chapID = ob.getChapterID2(chapterName);
//        int questinCOunt = ob.getQuestionCount2(chapID);
//        lblquestioncountJEE.setText("Cureent Que Count: " + questinCOunt);

            
            int chapterId = mastersortedDstChapterList.get(cmbToMasterChapter.getSelectedIndex()).getChapterId();
            String query = "SELECT COUNT(*) FROM MASTER_QUESTION_INFO WHERE CHAPTER_ID = ?";
            ps=dstConnection.prepareStatement(query);
            ps.setInt(1, chapterId);
            rs = ps.executeQuery();
                
            while(rs.next()) {
                count = rs.getInt(1);
            }
            
            
            int chapterId1 = sortedDstChapterList.get(cmbToChapter.getSelectedIndex()).getChapterId();
            query = "SELECT COUNT(*) FROM QUESTION_INFO WHERE CHAPTER_ID = ?";
            ps=dstConnection.prepareStatement(query);
            ps.setInt(1, chapterId1);
            rs = ps.executeQuery();
                
            while(rs.next()) {
                count1 = rs.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        lblquestioncountJEE.setText("Cureent Que Count: " + count +"( "+count1+")");
    }
    private static void copyFileUsingChannel(File source, File dest) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } finally {
            sourceChannel.close();
            destChannel.close();
        }
    }           
    
    private void copyFile(String sourcePath, String destPath) {
        File sourceFile = new File(sourcePath);
        File destFile = new File(destPath);
        FileChannel source = null;
        FileChannel destination = null;
        try {
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            source = new RandomAccessFile(sourceFile, "rw").getChannel();
            destination = new RandomAccessFile(destFile, "rw").getChannel();

            long position = 0;
            long count = source.size();

            source.transferTo(position, count, destination);
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private double getViewDimention(ArrayList<ImageRatioBean> imageRatioList,String path) {
        double returnValue = 0.0;
        for(ImageRatioBean imageRatioBean : imageRatioList) {
            if(imageRatioBean.getImageName().trim().equalsIgnoreCase(path)) {
                returnValue = imageRatioBean.getViewDimention();
                break;
            }
        }
        return returnValue;
    }

    private String replace(String question) {
//       if (question.contains("\"")) {
//            question = question.replace("\"", "\'");
//        }
        if (question.contains("\'")) {
            question = question.replace("\'", "\"");
        }

        if (question.contains("\"\"")) {
            question = question.replace("\"\"", "\"");
        }

        if (question.contains("\"\"")) {
            question = question.replace("\"\"", "\"");
        }

////        if (question.contains("'")) {
////            question = question.replace("'", "’");
////        }
//        
//        if (question.contains("’")) {
//            question = question.replace("’", "'");
//        }
        return question;
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        /*
        if (chkAddserially.isSelected() == true) {

            int chkchcount = jcomboCETChapter.getItemCount();
            int chkchcount1 = jcomboJEEChapter.getItemCount();

            if (chkchcount == chkchcount1) {
                for (int i = 18; i <= 34;) {

                    String chapterNameCET = jcomboCETChapter.getSelectedItem().toString();
                    chapterNameCET = chapterNameCET.substring(chapterNameCET.indexOf(")") + 1).trim();
                    DBCONNECTIONForJEEStu ob = new DBCONNECTIONForJEEStu();

                    String chapterNameJEE = jcomboJEEChapter.getSelectedItem().toString();
                    chapterNameJEE = chapterNameJEE.substring(chapterNameJEE.indexOf(")") + 1).trim();

                    int CETsubID = jcomboCETSubject.getSelectedIndex();
                    int CETchapID = ob.getChapterID1(chapterNameCET);

                    System.out.println("SID " + CETsubID + "  CID " + CETchapID);

                    int JEEsubID = jcomboJEESubject.getSelectedIndex();
                    int JEEchapID = ob.getChapterID2(chapterNameJEE);

                    AddData(CETsubID, CETchapID, JEEsubID, JEEchapID);

                    i++;
                    jcomboCETChapter.setSelectedIndex(i);
                    jcomboJEEChapter.setSelectedIndex(i);
                    repaint();

                }

            }

        } else if (chkRange.isSelected() == true) {

            from = Integer.parseInt(txtRangeFrom.getText().toString().trim());
            to = Integer.parseInt(txtRangeTo.getText().toString());
            String chapterNameCET = jcomboCETChapter.getSelectedItem().toString();
            chapterNameCET = chapterNameCET.substring(chapterNameCET.indexOf(")") + 1).trim();
            DBCONNECTIONForJEEStu ob = new DBCONNECTIONForJEEStu();

            String chapterNameJEE = jcomboJEEChapter.getSelectedItem().toString();
            chapterNameJEE = chapterNameJEE.substring(chapterNameJEE.indexOf(")") + 1).trim();

            int CETsubID = jcomboCETSubject.getSelectedIndex();
            int CETchapID = ob.getChapterID1(chapterNameCET);

            System.out.println("SID " + CETsubID + "  CID " + CETchapID);

            int JEEsubID = jcomboJEESubject.getSelectedIndex();
            int JEEchapID = ob.getChapterID2(chapterNameJEE);

            AddData(CETsubID, CETchapID, JEEsubID, JEEchapID);

        } else {
            String chapterNameCET = jcomboCETChapter.getSelectedItem().toString();
            chapterNameCET = chapterNameCET.substring(chapterNameCET.indexOf(")") + 1).trim();
            DBCONNECTIONForJEEStu ob = new DBCONNECTIONForJEEStu();

            String chapterNameJEE = jcomboJEEChapter.getSelectedItem().toString();
            chapterNameJEE = chapterNameJEE.substring(chapterNameJEE.indexOf(")") + 1).trim();

            int CETsubID = jcomboCETSubject.getSelectedIndex();
            int CETchapID = ob.getChapterID1(chapterNameCET);

            System.out.println("SID " + CETsubID + "  CID " + CETchapID);

            int JEEsubID = jcomboJEESubject.getSelectedIndex();
            int JEEchapID = ob.getChapterID2(chapterNameJEE);
            //          int JEEchapID = 23;

            AddData(CETsubID, CETchapID, JEEsubID, JEEchapID);

        } */
        
        AddData();
        AddMasterData();
        
    }//GEN-LAST:event_jButton1ActionPerformed

 private void AddMasterData() {
        int srcChapterId = sortedSrcChapterList.get(cmbFromChapter.getSelectedIndex()).getChapterId();
        int dstChapterId = mastersortedDstChapterList.get(cmbToMasterChapter.getSelectedIndex()).getChapterId();
        int dstQuestionId = new NewIdOperation().getNewMaxId("MASTER_QUESTION_INFO", "QUESTION_ID", dstConnection);
        int cnt = 0;
        ArrayList<QuestionBean> sortList = new ArrayList<QuestionBean>();
        for(QuestionBean questionsBean : sortedSrcQuestionList) {
            if(srcChapterId == questionsBean.getChapterId()) {
                sortList.add(questionsBean);
            }
        }
        
        String queImgPath = "";
        String optImgPath = "";
        String hintImgPath = "";
        ImageRatioBean imageRatioBean = null;
        ArrayList<ImageRatioBean> imageRatioList = new ArrayList<ImageRatioBean>();
        double viewDimention = 0.0;
        try{
            String query;
           
            for(QuestionBean questionBean : sortList) {
                imageRatioList.clear();
                query = "INSERT INTO MASTER_QUESTION_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps=dstConnection.prepareStatement(query);
                String que = replace(questionBean.getQuestion());
                String opA = replace(questionBean.getOptionA());
                String opB = replace(questionBean.getOptionB());
                String opC = replace(questionBean.getOptionC());
                String opD = replace(questionBean.getOptionD());
                String hint = replace(questionBean.getHint());
                ps.setInt(1, dstQuestionId);
                ps.setString(2, que.trim());
                ps.setString(3, opA.trim());
                ps.setString(4, opB.trim());
                ps.setString(5, opC.trim());
                ps.setString(6, opD.trim());
                ps.setString(7, questionBean.getAnswer());
                ps.setString(8, hint);
                ps.setInt(9, questionBean.getLevel());
                ps.setInt(10, questionBean.getSubjectId());
                ps.setInt(11, dstChapterId);
                ps.setInt(12, dstChapterId);
                
                if(questionBean.isIsQuestionAsImage()) {
                   
                    queImgPath = "masterImages/"+dstChapterId+"Question"+dstQuestionId+".png";  
//                    queImgPath = "images/"+dstChapterId+"Question"+dstQuestionId+".png";
                    ps.setBoolean(13, true);
                    copyFile(srcFileLocation+"/"+questionBean.getQuestionImagePath(), queImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getQuestionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(queImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(13, false);
                    queImgPath = "";
                }
                ps.setString(14, queImgPath);
        
                if(questionBean.isIsOptionAsImage()) {
                    optImgPath = "masterImages/"+dstChapterId+"Option"+dstQuestionId+".png";
//                    optImgPath = "images/"+dstChapterId+"Option"+dstQuestionId+".png";
                    ps.setBoolean(15, true);
                    copyFile(srcFileLocation+"/"+questionBean.getOptionImagePath(), optImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getOptionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(optImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(15, false);
                    optImgPath = "";
                }
//                ps.setBoolean(15, questionBean.isIsOptionAsImage());
                ps.setString(16, optImgPath);
                
                if(questionBean.isIsHintAsImage()) {
                    hintImgPath = "masterImages/"+dstChapterId+"Hint"+dstQuestionId+".png";
//                    hintImgPath = "images/"+dstChapterId+"Hint"+dstQuestionId+".png";
                    ps.setBoolean(17, true);
                    copyFile(srcFileLocation+"/"+questionBean.getHintImagePath(), hintImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getHintImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(hintImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(17, false);
                    hintImgPath = "";
                }
//                ps.setBoolean(17, questionBean.isIsHintAsImage());
                ps.setString(18, hintImgPath);
                ps.setInt(19, questionBean.getAttempt());
                ps.setInt(20, questionBean.getType());
                ps.setString(21, questionBean.getYear());
                ps.executeUpdate();
                
//                String a = MakeImage(opA, dstQuestionId, "a");
//                String b = MakeImage(opB, dstQuestionId, "b");
//                String c = MakeImage(opC, dstQuestionId, "c");
//                String d = MakeImage(opD, dstQuestionId, "d");
//                
//                query = "INSERT INTO MASTER_OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
//                ps=dstConnection.prepareStatement(query);
//                ps.setInt(1, dstQuestionId);
//                ps.setString(2, a);
//                ps.setString(3, b);
//                ps.setString(4, c);
//                ps.setString(5, d);
//                ps.executeUpdate();
                
                ImportOptionImageDimensionsBean optionBean = getOptionBean(srcOptionDimList,questionBean.getQuestionId());

                query = "INSERT INTO MASTER_OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
                ps=dstConnection.prepareStatement(query);
                ps.setInt(1, dstQuestionId);
                ps.setString(2, optionBean.getOptionA());
                ps.setString(3, optionBean.getOptionB());
                ps.setString(4, optionBean.getOptionC());
                ps.setString(5, optionBean.getOptionD());
                ps.executeUpdate();
                
                query = "INSERT INTO MASTER_IMAGERATIO VALUES(?,?)";
                for(ImageRatioBean ratioBean : imageRatioList) {
                    ps=dstConnection.prepareStatement(query);
                    ps.setString(1, ratioBean.getImageName());
                    ps.setDouble(2, ratioBean.getViewDimention());
                    ps.executeUpdate();
                }
                
                NewAndOldBean newAndOldBean = getNewAndOldBean(questionBean.getQuestionId());
                if(newAndOldBean != null) {
                    query = "INSERT INTO NEWANDOLDQUESTION VALUES(?,?)";
                    ps=dstConnection.prepareStatement(query);
                    ps.setInt(1, dstQuestionId);
                    ps.setInt(2, newAndOldBean.getQuestionType());
                    ps.executeUpdate();
                }
                cnt++;
                dstQuestionId++;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Total Questions:"+cnt);
        
    }    
    private void AddData() {
        int srcChapterId = sortedSrcChapterList.get(cmbFromChapter.getSelectedIndex()).getChapterId();
        int dstChapterId = sortedDstChapterList.get(cmbToChapter.getSelectedIndex()).getChapterId();
        int dstQuestionId = new NewIdOperation().getNewMaxId("QUESTION_INFO", "QUESTION_ID", dstConnection);
        int cnt = 0;
        ArrayList<QuestionBean> sortList = new ArrayList<QuestionBean>();
        for(QuestionBean questionsBean : sortedSrcQuestionList) {
            if(srcChapterId == questionsBean.getChapterId()) {
                sortList.add(questionsBean);
            }
        }
        
        String queImgPath = "";
        String optImgPath = "";
        String hintImgPath = "";
        ImageRatioBean imageRatioBean = null;
        ArrayList<ImageRatioBean> imageRatioList = new ArrayList<ImageRatioBean>();
        double viewDimention = 0.0;
        try{
            String query;
           
            for(QuestionBean questionBean : sortList) {
                imageRatioList.clear();
                query = "INSERT INTO QUESTION_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps=dstConnection.prepareStatement(query);
                String que = replace(questionBean.getQuestion());
                String opA = replace(questionBean.getOptionA());
                String opB = replace(questionBean.getOptionB());
                String opC = replace(questionBean.getOptionC());
                String opD = replace(questionBean.getOptionD());
                String hint = replace(questionBean.getHint());
                ps.setInt(1, dstQuestionId);
                ps.setString(2, que.trim());
                ps.setString(3, opA.trim());
                ps.setString(4, opB.trim());
                ps.setString(5, opC.trim());
                ps.setString(6, opD.trim());
                ps.setString(7, questionBean.getAnswer());
                ps.setString(8, hint);
                ps.setInt(9, questionBean.getLevel());
                ps.setInt(10, questionBean.getSubjectId());
                ps.setInt(11, dstChapterId);
                ps.setInt(12, dstChapterId);
                
                if(questionBean.isIsQuestionAsImage()) {
//                    queImgPath = "masterImages/"+dstChapterId+"Question"+dstQuestionId+".png";        
                    queImgPath = "images/"+dstChapterId+"Question"+dstQuestionId+".png";
                    ps.setBoolean(13, true);
                    copyFile(srcFileLocation+"/"+questionBean.getQuestionImagePath(), queImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getQuestionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(queImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(13, false);
                    queImgPath = "";
                }
                ps.setString(14, queImgPath);
        
                if(questionBean.isIsOptionAsImage()) {
//                    optImgPath = "masterImages/"+dstChapterId+"Option"+dstQuestionId+".png";
                    optImgPath = "images/"+dstChapterId+"Option"+dstQuestionId+".png";
                    ps.setBoolean(15, true);
                    copyFile(srcFileLocation+"/"+questionBean.getOptionImagePath(), optImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getOptionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(optImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(15, false);
                    optImgPath = "";
                }
//                ps.setBoolean(15, questionBean.isIsOptionAsImage());
                ps.setString(16, optImgPath);
                
                if(questionBean.isIsHintAsImage()) {
//                    hintImgPath = "masterImages/"+dstChapterId+"Hint"+dstQuestionId+".png";
                    hintImgPath = "images/"+dstChapterId+"Hint"+dstQuestionId+".png";
                    ps.setBoolean(17, true);
                    copyFile(srcFileLocation+"/"+questionBean.getHintImagePath(), hintImgPath);
                    viewDimention = getViewDimention(srcImageRatioList, questionBean.getHintImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(hintImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        imageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(17, false);
                    hintImgPath = "";
                }
//                ps.setBoolean(17, questionBean.isIsHintAsImage());
                ps.setString(18, hintImgPath);
                ps.setInt(19, questionBean.getAttempt());
                ps.setInt(20, questionBean.getType());
                ps.setString(21, questionBean.getYear());
                ps.executeUpdate();
                
//                String a = MakeImage(opA, dstQuestionId, "a");
//                String b = MakeImage(opB, dstQuestionId, "b");
//                String c = MakeImage(opC, dstQuestionId, "c");
//                String d = MakeImage(opD, dstQuestionId, "d");
//                
//                query = "INSERT INTO MASTER_OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
//                ps=dstConnection.prepareStatement(query);
//                ps.setInt(1, dstQuestionId);
//                ps.setString(2, a);
//                ps.setString(3, b);
//                ps.setString(4, c);
//                ps.setString(5, d);
//                ps.executeUpdate();
                
                ImportOptionImageDimensionsBean optionBean = getOptionBean(srcOptionDimList,questionBean.getQuestionId());

                query = "INSERT INTO OPTION_IMAGE_DIMENSIONS VALUES(?,?,?,?,?)";
                ps=dstConnection.prepareStatement(query);
                ps.setInt(1, dstQuestionId);
                ps.setString(2, optionBean.getOptionA());
                ps.setString(3, optionBean.getOptionB());
                ps.setString(4, optionBean.getOptionC());
                ps.setString(5, optionBean.getOptionD());
                ps.executeUpdate();
                
                query = "INSERT INTO IMAGERATIO VALUES(?,?)";
                for(ImageRatioBean ratioBean : imageRatioList) {
                    ps=dstConnection.prepareStatement(query);
                    ps.setString(1, ratioBean.getImageName());
                    ps.setDouble(2, ratioBean.getViewDimention());
                    ps.executeUpdate();
                }
                
                NewAndOldBean newAndOldBean = getNewAndOldBean(questionBean.getQuestionId());
                if(newAndOldBean != null) {
                    query = "INSERT INTO NEWANDOLDQUESTION VALUES(?,?)";
                    ps=dstConnection.prepareStatement(query);
                    ps.setInt(1, dstQuestionId);
                    ps.setInt(2, newAndOldBean.getQuestionType());
                    ps.executeUpdate();
                }
                cnt++;
                dstQuestionId++;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Total Questions:"+cnt);
        
    }    
    private NewAndOldBean getNewAndOldBean(int questionId) {
        NewAndOldBean returnBean = null;
        if(srcNewAndOldList != null) {
            for(NewAndOldBean bean : srcNewAndOldList) {
                if(bean.getQuestionId() == questionId) {
                    returnBean = bean;
                    break;
                }
            }
        }
        return returnBean;
    }
    
    private ImportOptionImageDimensionsBean getOptionBean(ArrayList<ImportOptionImageDimensionsBean> optionDimList,int quesId) {
        ImportOptionImageDimensionsBean returnBean = null;
        for(ImportOptionImageDimensionsBean dimensionsBean : optionDimList) {
            if(dimensionsBean.getQuestionId() == quesId) {
                returnBean = dimensionsBean;
                break;
            }
        }
        return returnBean;
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ImportDataProcess.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ImportDataProcess.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ImportDataProcess.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ImportDataProcess.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ImportDataProcess().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel MainBodyPanel;
    private javax.swing.JPanel SubBodyPanel;
    private javax.swing.JPanel TitlePanel;
    private javax.swing.JButton btnFromQCount;
    private javax.swing.JButton btnToQCount;
    private javax.swing.JCheckBox chkBoxLink;
    private javax.swing.JComboBox<String> cmbFromChapter;
    private javax.swing.JComboBox<String> cmbFromSubject;
    private javax.swing.JComboBox<String> cmbToChapter;
    private javax.swing.JComboBox<String> cmbToMasterChapter;
    private javax.swing.JComboBox<String> cmbToSubject;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblNewData;
    private javax.swing.JLabel lblOldData;
    private javax.swing.JLabel lblquestioncountCET;
    private javax.swing.JLabel lblquestioncountJEE;
    // End of variables declaration//GEN-END:variables
}
