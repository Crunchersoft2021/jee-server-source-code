/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NewRegistrationForm.java
 *
 * Created on Aug 1, 2014, 2:50:03 PM
 */
package com.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.bean.ChapterBean;
import com.bean.SubjectBean;
import com.db.operations.ChapterOperation;
import javax.swing.JTable;
import com.Model.TitleInfo;

/**
 *
 * @author SAGAR
 */
public class WeightageSettings extends javax.swing.JFrame {

    private HomePage homePage;
    private DefaultTableCellRenderer centerRenderer;
    private int subjectFirstId = 0,subjectSecondId = 0,subjectThirdId = 0;
    private ArrayList<ChapterBean> chapterList;
    private ArrayList<ChapterBean> updatedChapterList;

     public WeightageSettings()
    {
        initComponents();
    }
    public WeightageSettings(ArrayList<SubjectBean> subjectList,HomePage homePage){
        initComponents();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        MainPanel.setSize(screenSize);
        this.homePage = homePage;
        this.setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        setTableSettings(SubFirstTable);
        setTableSettings(SubSecondTable);
        setTableSettings(SubThirdTable);
        int index = 0;
        for(SubjectBean subjectBean : subjectList) {
            if(index == 0)
                subjectFirstId = subjectBean.getSubjectId();
            else if(index == 1)
                subjectSecondId = subjectBean.getSubjectId();
            else if(index == 2)
                subjectThirdId = subjectBean.getSubjectId();
            SubBodyTabPane.setTitleAt(index, subjectBean.getSubjectName());
            index++;
        }
        chapterList = new ChapterOperation().getChapterList(subjectList);
        getContentPane().setBackground(new Color(0, 100, 50));
        ArrayList<Object[]> subjectFirstChapterList = new ArrayList<>();
        ArrayList<Object[]> subjectSecondChapterList = new ArrayList<>();
        ArrayList<Object[]> subjectThirdChapterList = new ArrayList<>();
        int srNo1 = 1,srNo2 = 1,srNo3 = 1;
        int subFirstCount = 0,subSecondCount = 0,subThirdCount = 0;
        updatedChapterList = null;
        for(ChapterBean chapterBean : chapterList) {
            if(chapterBean.getSubjectId() == subjectFirstId) {
                subjectFirstChapterList.add(new Object[]{srNo1++,chapterBean.getChapterName(),chapterBean.getWeightage()});
                subFirstCount += chapterBean.getWeightage();
            } else if(chapterBean.getSubjectId() == subjectSecondId) {
                subjectSecondChapterList.add(new Object[]{srNo2++,chapterBean.getChapterName(),chapterBean.getWeightage()});
                subSecondCount += chapterBean.getWeightage();
            } else if(chapterBean.getSubjectId() == subjectThirdId) {
                subjectThirdChapterList.add(new Object[]{srNo3++,chapterBean.getChapterName(),chapterBean.getWeightage()});
                subThirdCount += chapterBean.getWeightage();
            }
        }
        
        /*for(ChapterBean chapterBean : chapterList) {
            int weight = 0;
            for(WeightageBean weightageBean : weightageList) {
                if(chapterBean.getChapterId() == weightageBean.getChapterId()) {
                    weight = weightageBean.getWeightage();
                    break;
                }
            }
            if(chapterBean.getSubjectId() == subjectFirstId) {
                subjectFirstChapterList.add(new Object[]{srNo1,chapterBean.getChapterName(),weight});
                subFirstCount += weight;
                srNo1++;
            } else if(chapterBean.getSubjectId() == subjectSecondId) {
                subjectSecondChapterList.add(new Object[]{srNo2,chapterBean.getChapterName(),weight});
                subSecondCount += weight;
                srNo2++;
            } else if(chapterBean.getSubjectId() == subjectThirdId) {
                subjectThirdChapterList.add(new Object[]{srNo3,chapterBean.getChapterName(),weight});
                subThirdCount += weight;
                srNo3++;
            }
        }*/
        
        loadsWeightage(subjectFirstChapterList,SubFirstTable,subFirstCount);
        loadsWeightage(subjectSecondChapterList,SubSecondTable,subSecondCount);
        loadsWeightage(subjectThirdChapterList,SubThirdTable,subThirdCount);
    }
    
    private void setTableSettings(JTable table){
        table.getTableHeader().setDefaultRenderer(centerRenderer);
        table.getTableHeader().setForeground(Color.black);
        table.getTableHeader().setBackground(new Color(57, 97, 152));
        table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        table.getTableHeader().setFont(new Font("Microsoft JhengHei", Font.PLAIN, 14));
    }
    
    private void loadsWeightage(ArrayList<Object[]> chapterInfoList,JTable table, int weightageCount) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int count = model.getRowCount();
        if (count > 0) {
            for (int i = count - 1; i >= 0; i--) {
                model.removeRow(i);
            }
        }
        if (chapterInfoList != null) {
            if (chapterInfoList.size() > 0) {
                Iterator<Object[]> it = chapterInfoList.iterator();
                while (it.hasNext()) {
                    model.addRow(it.next());
                }
            }
        }
        model.addRow(new Object[]{"","",""});
        model.addRow(new Object[]{"", " Total " , weightageCount });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MainPanel = new javax.swing.JPanel();
        LblSettings = new javax.swing.JLabel();
        BtnBack = new javax.swing.JButton();
        BodyPanelScrollPane = new javax.swing.JScrollPane();
        BodyPanel = new javax.swing.JPanel();
        BtnSaveSettings = new javax.swing.JButton();
        LblMessage1 = new javax.swing.JLabel();
        LblMessage2 = new javax.swing.JLabel();
        SubBodyTabPane = new javax.swing.JTabbedPane();
        SubFirstScrollPane = new javax.swing.JScrollPane();
        SubFirstTable = new javax.swing.JTable();
        SubSecondScrollPane = new javax.swing.JScrollPane();
        SubSecondTable = new javax.swing.JTable();
        SubThirdScrollPane = new javax.swing.JScrollPane();
        SubThirdTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("JEE Preaparation Software 2015");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        MainPanel.setBackground(new java.awt.Color(0, 102, 102));
        MainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        MainPanel.setName("MainPanel"); // NOI18N

        LblSettings.setFont(new java.awt.Font("Microsoft JhengHei", 0, 36)); // NOI18N
        LblSettings.setForeground(new java.awt.Color(255, 255, 255));
        LblSettings.setText("Settings");
        LblSettings.setName("LblSettings"); // NOI18N

        BtnBack.setBackground(new java.awt.Color(0, 20, 72));
        BtnBack.setFont(new java.awt.Font("Microsoft JhengHei", 1, 16)); // NOI18N
        BtnBack.setForeground(new java.awt.Color(255, 255, 255));
        BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png"))); // NOI18N
        BtnBack.setBorder(null);
        BtnBack.setContentAreaFilled(false);
        BtnBack.setName("BtnBack"); // NOI18N
        BtnBack.setPreferredSize(new java.awt.Dimension(60, 60));
        BtnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnBackMouseExited(evt);
            }
        });
        BtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBackActionPerformed(evt);
            }
        });

        BodyPanelScrollPane.setName("BodyPanelScrollPane"); // NOI18N

        BodyPanel.setBackground(new java.awt.Color(11, 45, 55));
        BodyPanel.setForeground(new java.awt.Color(255, 255, 255));
        BodyPanel.setName("BodyPanel"); // NOI18N

        BtnSaveSettings.setBackground(new java.awt.Color(0, 102, 102));
        BtnSaveSettings.setFont(new java.awt.Font("Microsoft JhengHei", 1, 16)); // NOI18N
        BtnSaveSettings.setForeground(new java.awt.Color(255, 255, 255));
        BtnSaveSettings.setText("Save Settings");
        BtnSaveSettings.setBorder(null);
        BtnSaveSettings.setName("BtnSaveSettings"); // NOI18N
        BtnSaveSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSaveSettingsActionPerformed(evt);
            }
        });

        LblMessage1.setFont(new java.awt.Font("Microsoft JhengHei", 0, 16)); // NOI18N
        LblMessage1.setForeground(new java.awt.Color(255, 255, 255));
        LblMessage1.setText("Chapter Weightage for Test");
        LblMessage1.setName("LblMessage1"); // NOI18N

        LblMessage2.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        LblMessage2.setForeground(new java.awt.Color(255, 51, 51));
        LblMessage2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMessage2.setText("Double click on the field which you want to change weightage.");
        LblMessage2.setName("LblMessage2"); // NOI18N

        SubBodyTabPane.setName("SubBodyTabPane"); // NOI18N

        SubFirstScrollPane.setName("SubFirstScrollPane"); // NOI18N

        SubFirstTable.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubFirstTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Weightage in Percent"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubFirstTable.setName("SubFirstTable"); // NOI18N
        SubFirstTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SubFirstTableMouseClicked(evt);
            }
        });
        SubFirstScrollPane.setViewportView(SubFirstTable);

        SubBodyTabPane.addTab("Physics", SubFirstScrollPane);

        SubSecondScrollPane.setName("SubSecondScrollPane"); // NOI18N

        SubSecondTable.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubSecondTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Weightage in Percent"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubSecondTable.setName("SubSecondTable"); // NOI18N
        SubSecondTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SubSecondTableMouseClicked(evt);
            }
        });
        SubSecondScrollPane.setViewportView(SubSecondTable);

        SubBodyTabPane.addTab("Chemistry", SubSecondScrollPane);

        SubThirdScrollPane.setName("SubThirdScrollPane"); // NOI18N

        SubThirdTable.setFont(new java.awt.Font("Times New Roman", 1, 11)); // NOI18N
        SubThirdTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(1), "Physics",  new Integer(50)},
                { new Integer(2), "Chemistry",  new Integer(0)},
                { new Integer(3), "Maths",  new Integer(0)},
                { new Integer(4), "PCM",  new Integer(50)}
            },
            new String [] {
                "Chapter Id", "Chapter Name", "Weightage in Percent"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SubThirdTable.setName("SubThirdTable"); // NOI18N
        SubThirdTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SubThirdTableMouseClicked(evt);
            }
        });
        SubThirdScrollPane.setViewportView(SubThirdTable);

        SubBodyTabPane.addTab("Mathematics", SubThirdScrollPane);

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LblMessage2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(SubBodyTabPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, BodyPanelLayout.createSequentialGroup()
                        .addComponent(LblMessage1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BtnSaveSettings, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblMessage1)
                    .addComponent(BtnSaveSettings, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblMessage2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(SubBodyTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addContainerGap())
        );

        BodyPanelScrollPane.setViewportView(BodyPanel);

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(LblSettings)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addComponent(BodyPanelScrollPane)
                        .addGap(25, 25, 25))))
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(LblSettings))
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(BtnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25)
                .addComponent(BodyPanelScrollPane)
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
//    if(homePage != null)
//        homePage.dispose();
//    new HomePage().setVisible(true);
    this.dispose();
}//GEN-LAST:event_formWindowClosing

private void BtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBackActionPerformed
    this.dispose();
}//GEN-LAST:event_BtnBackActionPerformed

private void BtnBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseEntered
    BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-2.png"))); // NOI18N
}//GEN-LAST:event_BtnBackMouseEntered

private void BtnBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnBackMouseExited
    BtnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Back-1.png")));
}//GEN-LAST:event_BtnBackMouseExited

    private void BtnSaveSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSaveSettingsActionPerformed
        DefaultTableModel modelFirstSubject = (DefaultTableModel) SubFirstTable.getModel();
        Vector rowsFirstSubject = modelFirstSubject.getDataVector();
        int subFirstChapCount = getTableRowCount(rowsFirstSubject);
        
        DefaultTableModel modelSecondSubject = (DefaultTableModel) SubSecondTable.getModel();
        Vector rowsSecondSubject = modelSecondSubject.getDataVector();
        int subSecondChapCount = getTableRowCount(rowsSecondSubject);
        
        DefaultTableModel modelThirdSubject = (DefaultTableModel) SubThirdTable.getModel();
        Vector rowsThirdSubject = modelThirdSubject.getDataVector();
        int subThirdChapCount = getTableRowCount(rowsThirdSubject);

        if(subFirstChapCount == 100 && subSecondChapCount == 100 && subThirdChapCount == 100) {
            
            setUpadatedRows(rowsFirstSubject);
            setUpadatedRows(rowsSecondSubject);
            setUpadatedRows(rowsThirdSubject);

            if(updatedChapterList != null) {
                new ChapterOperation().updateChapterWeightage(updatedChapterList);
                JOptionPane.showMessageDialog(rootPane, "Setting Saved Successfully");
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(rootPane, "No Updation Found");
            }
//            if(updatedWeightageList.size() != 0) {
//                new WeightageOperation().updateChapterWeightage(updatedWeightageList);
//                JOptionPane.showMessageDialog(rootPane, "Setting Saved Successfully");
//                this.dispose();
//            } else {
//                JOptionPane.showMessageDialog(rootPane, "No Updation Found");
//            }
        } else {
            String message = "";
            if (subFirstChapCount != 100 && subSecondChapCount !=100 && subThirdChapCount !=100) {
                message = "All";
            } else if(subFirstChapCount != 100) {
                if(subSecondChapCount != 100)
                    message = "First & Second";
                else if (subThirdChapCount != 100)
                    message = "First & Third";
                else
                    message = "First";
            } else if (subSecondChapCount != 100) {
                if(subThirdChapCount != 100)
                    message = "Second & Third";
                else
                    message = "Second";
            } else {
                message = "Third";
            }
            JOptionPane.showMessageDialog(rootPane,message + " Subject Percentage is invalid.");
            this.dispose();
        }
    }//GEN-LAST:event_BtnSaveSettingsActionPerformed

    private void SubFirstTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SubFirstTableMouseClicked
        // TODO add your handling code here:
        changeCellValue(evt);
    }//GEN-LAST:event_SubFirstTableMouseClicked

    private void SubSecondTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SubSecondTableMouseClicked
        // TODO add your handling code here:
        changeCellValue(evt);
    }//GEN-LAST:event_SubSecondTableMouseClicked

    private void SubThirdTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SubThirdTableMouseClicked
        // TODO add your handling code here:
        changeCellValue(evt);
    }//GEN-LAST:event_SubThirdTableMouseClicked

    private int getTableRowCount(Vector rows){
        int count = 0;
        for(int i=0; i< rows.size()-2; i++) {
            Vector row = (Vector) rows.elementAt(i);
            count+= (Integer) row.elementAt(2);
        }
        return count;
    }
 
    private void setUpadatedRows(Vector rows){
        for (int i = 0; i < rows.size()-2 ; i++) {
            Vector row = (Vector) rows.elementAt(i);
            ChapterBean cb = null;
            for(ChapterBean chapterBean : chapterList) {
                if(chapterBean.getChapterName().equals(row.elementAt(1).toString())) {
                    if(chapterBean.getWeightage() != (Integer) row.elementAt(2)) {
                        cb = new ChapterBean();
                        cb.setChapterId(chapterBean.getChapterId());
                        cb.setSubjectId(chapterBean.getSubjectId());
                        cb.setChapterName(chapterBean.getChapterName().trim());
                        cb.setWeightage((Integer) row.elementAt(2));
                        if(updatedChapterList == null)
                            updatedChapterList = new ArrayList<ChapterBean>();
                        updatedChapterList.add(cb);
                    }
                }
            }
        }
    }
    
    public void changeCellValue(java.awt.event.MouseEvent evt) 
    {
        if (evt.getClickCount() == 2) 
        {
            final JTable target = (JTable) evt.getSource();
            final int row = target.getSelectedRow();
            
            if (row < target.getRowCount()-2) 
            {
                String chapName;
                int oldValue;
                chapName = (String)target.getValueAt(row,1);
                oldValue = (Integer) target.getValueAt(row, 2);
                int returnValue = getDynamicCount(oldValue,chapName);
                if(returnValue != oldValue) 
                {
                    target.setValueAt(returnValue, row, 2);
                    setTotalCount(target);
                }
            }
        }
    }
    
    private int getDynamicCount(int oldValue,String chapName){
        int returnValue;
        String input = "";
        try{
            input = JOptionPane.showInputDialog(this, "Enter Your Weightage.",chapName, JOptionPane.QUESTION_MESSAGE);
            returnValue = Integer.parseInt(input);
            if(returnValue <= 0) {
                returnValue = oldValue;
                JOptionPane.showMessageDialog(this, "Please Enter Valid Weightage.");
            }
        }catch(Exception e){
            if(!(input == null || (input != null && ("".equals(input)))))
                JOptionPane.showMessageDialog(this, "Please Enter Only Numbers.");
            
            returnValue = oldValue;
        }
        return returnValue;
    }
    
    private void setTotalCount(JTable table){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        Vector rows = model.getDataVector();
        int count = 0;
        int size = rows.size();
        for(int i=0;i<size-2;i++){
            Vector row = (Vector) rows.elementAt(i);
            int value = (Integer) row.elementAt(2);
            count += value;
        }
        model.removeRow(size-1);
        model.addRow(new Object[]{"","Total",count});
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WeightageSettings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WeightageSettings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WeightageSettings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WeightageSettings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JScrollPane BodyPanelScrollPane;
    private javax.swing.JButton BtnBack;
    private javax.swing.JButton BtnSaveSettings;
    private javax.swing.JLabel LblMessage1;
    private javax.swing.JLabel LblMessage2;
    private javax.swing.JLabel LblSettings;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JTabbedPane SubBodyTabPane;
    private javax.swing.JScrollPane SubFirstScrollPane;
    private javax.swing.JTable SubFirstTable;
    private javax.swing.JScrollPane SubSecondScrollPane;
    private javax.swing.JTable SubSecondTable;
    private javax.swing.JScrollPane SubThirdScrollPane;
    private javax.swing.JTable SubThirdTable;
    // End of variables declaration//GEN-END:variables
}