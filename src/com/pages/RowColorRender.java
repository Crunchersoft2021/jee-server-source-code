/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pages;

import com.bean.ClassTestBean;
import com.db.operations.ClassSaveTestOperation;
import java.awt.Color;
import java.awt.Component;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Aniket
 */
public class RowColorRender extends DefaultTableCellRenderer {
    
     ArrayList<ClassTestBean> tempclassTestList = new ArrayList<ClassTestBean>();
      public  JTable getNewRenderedTable( JTable jTable1) 
  {
         
        jTable1.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

            public Component getTableCellRendererComponent(JTable jTable1, Object value, boolean isSelected,boolean hasFocus, int row, int column)
            {
                tempclassTestList = new ClassSaveTestOperation().getTempClassTestInfo(); 
                 if (value instanceof Integer && column == 0) {
                        row = jTable1.convertRowIndexToModel(row);
                          int testid1 = (int) jTable1.getModel().getValueAt(row, column);
                          
                       if (testid1==tempclassTestList.get(0).getTestId()) {                       
                            setForeground(Color.red);
                            setBackground(Color.white);
                        } else {
                           setForeground(Color.black);
                           setBackground(Color.yellow);
                        }
                    return super.getTableCellRendererComponent(jTable1, value, isSelected, hasFocus, row, column);
                }
                else {
                setForeground(null);
                return super.getTableCellRendererComponent(jTable1, value, isSelected, hasFocus, row, column);
                }
            }

        });
   return jTable1;
        
    }
    
}
