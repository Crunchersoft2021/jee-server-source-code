/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ui.support.pages;

import com.Model.TitleInfo;
import com.Model.SetTypeLevel;
import com.Registration.Registration;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.DbConnection;
import com.db.operations.SubjectOperation;
import com.pages.HomePage;
import com.pattern.operations.PatternOperation;
import com.pattern.operations.WeightageManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static org.icepdf.ri.common.FileExtensionUtils.ps;

/**
 *
 * @author Aniket
 */
public class ProcessingPanel extends javax.swing.JFrame {

    /**
     * Creates new form ProcessingPanel
     */
    private HomePage homePage;
    private Registration registration;
     private Connection conn = null;
    private ResultSet rs1 = null;
     private ResultSet rs = null;
    private PreparedStatement ps1 = null; 
     private PreparedStatement ps = null; 
    public ProcessingPanel(HomePage homePage) {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.homePage = homePage;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                process();    
            }
        });
    }
    
    public ProcessingPanel(Registration registration) {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.registration = registration;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                initialProcess();
            }
        });
    }
    
    public void process() {
        
        System.out.println("------------Process--------------");
        boolean returnValue = new PatternOperation().sortQuestions(homePage.getPatternIndexValue());
        System.out.println("-------------after sort----------------");
        if(returnValue) {
            new WeightageManager().setAllWieghtage(homePage.getSubjectList());
//            new RemoveUnwantedCode().RefineData();
            new SetTypeLevel().dataRefining();
            this.setVisible(false);
            JOptionPane.showMessageDialog(null, "Pattern Changed Successfully.");
        } else {
            this.setVisible(false);
            JOptionPane.showMessageDialog(null, "Error In Pattern Changing.");
        }
        this.dispose();
        homePage.setEnabled(true);
    }

    public void initialProcess() {
//       conn=new DbConnection().getConnection();
//       
//        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
//        try {
//             
//            String query = "Select * from Question_Info where QUESTION_TYPE=? and subject_id=?";
//                     ps=conn.prepareStatement(query);
//                     ps.setInt(1, 2);
//                     //ps.setInt(2, rs1.getInt("QUESTION_ID"));
//                     ps.setInt(2, 3);
//                  
//                   rs1=ps.executeQuery();
//             System.out.println("-------------query------------"+query);
//            System.out.println("-------------data copy----------------");
//        
//            while (rs1.next()) {
//               String query1 = "UPDATE MASTER_QUESTION_INFO SET QUESTION_TYPE = ?, NUMERICAL_ANSWER=?, UNIT=? WHERE QUESTION=? and  subject_id=?";
//                     ps1=conn.prepareStatement(query1);
//                     ps1.setInt(1,2);
//                     ps1.setString(2, rs1.getString("NUMERICAL_ANSWER"));
//                     ps1.setString(3, rs1.getString("UNIT"));
//                     ps1.setString(4, rs1.getString("QUESTION"));
//                     ps1.setInt(5, 3);
//                    
//                   
//                     int  kl=ps1.executeUpdate();
//                     System.out.println(kl+" records affected.......................................................");  
//                    System.out.println("-------------In loop----------------"); 
//            }
//           
//            System.out.println("-------------after data copy---------------");
//        } catch (Exception ex) {
//            returnList.clear();
//            ex.printStackTrace();
//        } finally {
//            
//        }
//        
        
        
        boolean returnValue = new PatternOperation().sortQuestions(0);
        ArrayList<SubjectBean> subjectList = new SubjectOperation().getSubjectsList();
        if(returnValue) {
            new WeightageManager().setAllWieghtage(subjectList);
     //      new RemoveUnwantedCode().RefineData();
            new SetTypeLevel().dataRefining();
            this.setVisible(false);
            JOptionPane.showMessageDialog(null, "Software Ready For Use.");
        } else {
            this.setVisible(false);
            JOptionPane.showMessageDialog(null, "Error In Pattern Changing.");
        }
        this.dispose();
        registration.setEnabled(true);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Processing.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
