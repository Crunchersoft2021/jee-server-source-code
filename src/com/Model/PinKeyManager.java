/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.ProductBean;
import com.bean.RegistrationBean;
import com.db.operations.ProductOperation;

import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class PinKeyManager {

    public String getPinKey(RegistrationBean infoBean,String activationDate) {
        HashCodeGenerator hc = new HashCodeGenerator();
        String productName = infoBean.getProductBean().getProductName() + " " + infoBean.getProductBean().getProductTypeBean().getProductType();
        String firstPinHash = productName + infoBean.getCustMobile().trim() + activationDate;
        firstPinHash = firstPinHash.toLowerCase();
        firstPinHash = hc.pinHash(firstPinHash, "MD5") + hc.pinHash(firstPinHash, "MD2") + hc.pinHash(firstPinHash, "SHA1");

        String secondPinHash = infoBean.getCpuId().trim() + productName + activationDate;
        secondPinHash = secondPinHash.toLowerCase();
        secondPinHash = hc.pinHash(secondPinHash, "MD5") + hc.pinHash(secondPinHash, "MD2") + hc.pinHash(secondPinHash, "SHA1");

        String thirdPinHash = productName + activationDate + infoBean.getCustMobile().trim()+infoBean.getCpuId().trim();
        thirdPinHash = thirdPinHash.toLowerCase();
        thirdPinHash = hc.pinHash(thirdPinHash, "MD5") + hc.pinHash(thirdPinHash, "MD2") + hc.pinHash(thirdPinHash, "SHA1");

        String returnValue = getFirstKey(firstPinHash) + "-" + getSecondKey(secondPinHash) + "-" + getThirdKey(thirdPinHash);
        returnValue = changeChars(returnValue).toUpperCase();

        return returnValue;
    }

    private String getFirstKey(String hashValue) {
        String returnVal = ""
                + hashValue.charAt(30)
                + hashValue.charAt(47)
                + hashValue.charAt(71)
                + hashValue.charAt(38)
                + hashValue.charAt(62)
                + hashValue.charAt(9);
        return returnVal;
    }

    private String getSecondKey(String hashValue) {
        String returnVal = ""
                + hashValue.charAt(34)
                + hashValue.charAt(57)
                + hashValue.charAt(72)
                + hashValue.charAt(27)
                + hashValue.charAt(45)
                + hashValue.charAt(4);
        return returnVal;
    }

    private String getThirdKey(String hashValue) {
        String returnVal = ""
                + hashValue.charAt(28)
                + hashValue.charAt(94)
                + hashValue.charAt(88)
                + hashValue.charAt(10)
                + hashValue.charAt(55)
                + hashValue.charAt(13);
        return returnVal;
    }

    private String changeChars(String str) {
        ArrayList<Character> chIndexList = new ArrayList<Character>();
        for (int i = 0; i < 10; i++) {
            chIndexList.add(Character.forDigit(i, 10));
        }
        chIndexList.add('a');
        chIndexList.add('b');
        chIndexList.add('c');
        chIndexList.add('d');
        chIndexList.add('e');
        chIndexList.add('f');

        ArrayList<Character> chCharList = new ArrayList<Character>();
        chCharList.add('s');
        chCharList.add('b');
        chCharList.add('p');
        chCharList.add('8');
        chCharList.add('t');
        chCharList.add('k');
        chCharList.add('q');
        chCharList.add('r');
        chCharList.add('w');
        chCharList.add('9');
        chCharList.add('c');
        chCharList.add('j');
        chCharList.add('y');
        chCharList.add('2');
        chCharList.add('g');
        chCharList.add('u');

        String returnVal = "";
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch != '-') {
                int index = chIndexList.indexOf(ch);
                returnVal += "" + chCharList.get(index);
            } else {
                returnVal += "-";
            }
        }

        return returnVal;
    }

//    public static void main(String[] args) {
//        RegistrationBean infoBean = new RegistrationBean();
//        ProductBean pb = new ProductOperation().getProductBean(2);
//        infoBean.setProductBean(pb);
//        infoBean.setCpuId("29CP-9SBJ-GWWP-P9WR");
//        infoBean.setCustMobile("8975626060");
//        infoBean.setActivationDate("24-11-17");
//        System.out.println(new PinKeyManager().getPinKey(infoBean));
//    }
}
