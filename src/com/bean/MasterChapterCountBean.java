/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class MasterChapterCountBean {
    private int chapterId;
    private int totalQuetions;

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getTotalQuetions() {
        return totalQuetions;
    }

    public void setTotalQuetions(int totalQuetions) {
        this.totalQuetions = totalQuetions;
    }
}
