/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class CountChapterBean {
    private ChapterBean chapterBean;
    private int totalQuestions;

    public ChapterBean getChapterBean() {
        return chapterBean;
    }

    public void setChapterBean(ChapterBean chapterBean) {
        this.chapterBean = chapterBean;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }
    
}
