/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class SMSBean {
    
   private int sid, Packages, Send, Remaining;

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getPackages() {
        return Packages;
    }

    public void setPackages(int Packages) {
        this.Packages = Packages;
    }

    public int getSend() {
        return Send;
    }

    public void setSend(int Send) {
        this.Send = Send;
    }

    public int getRemaining() {
        return Remaining;
    }

    public void setRemaining(int Remaining) {
        this.Remaining = Remaining;
    }
   
    
}
