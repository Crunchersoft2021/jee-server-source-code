/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class TestingSettingBean {
    private int TID;
    private String TestingType;
    private Boolean Status;

    public int getTID() {
        return TID;
    }

    public void setTID(int TID) {
        this.TID = TID;
    }

    public String getTestingType() {
        return TestingType;
    }

    public void setTestingType(String TestingType) {
        this.TestingType = TestingType;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean Status) {
        this.Status = Status;
    }
    
    
}
