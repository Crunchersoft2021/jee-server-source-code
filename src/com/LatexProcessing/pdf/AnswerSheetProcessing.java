/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

import com.bean.QuestionBean;
import com.db.operations.QuestionOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class AnswerSheetProcessing {
    
    public String getAnswerSheet(ArrayList <QuestionBean> selectedQuestionList) {
        String completeAnswerSheeet = "";
        String column = "cc|";
        String start = "\\begin{longtable}{|";
        String end = "\\end{longtable}";
        int index = 1;
        
        if(selectedQuestionList.size() < 100){
            for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 12 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 12 != 0){
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 12;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else if(selectedQuestionList.size() < 1000) {
            for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 10 == 0 )
                    completeAnswerSheeet += "\\\\ \\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 10;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        } else {
            for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : selectedQuestionList){
                completeAnswerSheeet += index +"&"+ questionsBean.getAnswer();
                if(index % 10 == 0 )
                    completeAnswerSheeet += "\\hline";
                else 
                    completeAnswerSheeet +=  " & ";
                index++;
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 11;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
        }
//        System.out.println(completeAnswerSheeet);
        return completeAnswerSheeet;
    }
    
    
    
         public void generate1(int col,ArrayList<QuestionBean> list, String completeAnswerSheeet,String column,String start,String end,int index,int startNo){
          
                for(int i=0; i<10; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : list){
                if(questionsBean.getType()==0)
                {      
                    
                    System.out.println("po..........................................................................ddff");
                    
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 10 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }else if(questionsBean.getType()==1)
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 10 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }else if(questionsBean.getType()==0 || questionsBean.getType()==1 )
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 10 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }else if(questionsBean.getType()==2)
                {
                    
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getNumericalAnswer()+""+questionsBean.getUnit();
                    if(index % 10 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                    
                }
            }
            if((index-1) % 12 != 0){
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = list.size() % 12;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
             
             
         }
         
    
    
    
    
    
   public String getAnswerSheet(ArrayList <QuestionBean> selectedQuestionList,int startNo) {
       
        String mainAnswerSheet="";
            
       ArrayList<QuestionBean> list1=new ArrayList<QuestionBean>();
        ArrayList<QuestionBean> list2=new ArrayList<QuestionBean>();
        
        for(int i=0;i<selectedQuestionList.size();i++){
            
            if(selectedQuestionList.get(i).getType()==0 || selectedQuestionList.get(i).getType()==1){
                list1.add(selectedQuestionList.get(i));
                
            }else{
                
               list2.add(selectedQuestionList.get(i));  
            }
            
        }
     
        String completeAnswerSheeet = "";
        String column = "cc|";
        String start = "\\begin{longtable}{|";
        String end = "\\end{longtable}";
        int index = 1;
        int flag = 0;
        if(selectedQuestionList.size() < 100 && startNo < 100) 
            flag = 1;
        else if(selectedQuestionList.size() < 100 && startNo < 1000) 
            flag = 2;
        else if(selectedQuestionList.size() < 100 && startNo >= 1000) 
            flag = 3;
        else if(selectedQuestionList.size() < 1000 && startNo < 1000)
            flag = 2;
        else 
            flag = 3;
        if(flag == 1){
            // generate1(5,list1,completeAnswerSheeet,column,start,end,index,startNo);
                 for(int i=0; i<12; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : list1){
                if(questionsBean.getType()==0)
                {      
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }else if(questionsBean.getType()==1)
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }else if(questionsBean.getType()==0 || questionsBean.getType()==1 )
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }
            }
            if((index-1) % 12 != 0){
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = list1.size() % 12;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
            
            
            // for new numericals
            
           
            mainAnswerSheet+=completeAnswerSheeet;
            
        
        String column1 = "cc|";
        String start1 = "\\begin{longtable}{|";
        String end1 = "\\end{longtable}";
        int index1 = 1;
            
                for(int i=0; i<7; i++)
                start1 += column1;
            start1 += "} \\hline";
            mainAnswerSheet += start1;
            for(QuestionBean questionsBean : list2){
             if(questionsBean.getType()==2)
                {
                    
                    mainAnswerSheet += (startNo++) +"&"+ questionsBean.getNumericalAnswer()+""+questionsBean.getUnit();
                    if(index1 % 7 == 0 )
                        mainAnswerSheet += "\\hline";
                    else 
                        mainAnswerSheet +=  " & ";
                    index1++;
                    
                }
            }
            if((index1-1) % 8 != 0){
                mainAnswerSheet = mainAnswerSheet.substring(0, mainAnswerSheet.length()-2);
                int modValue = list2.size() % 7;
                mainAnswerSheet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end1;
            } else {
                mainAnswerSheet += end1;
            }
          
        } else if(flag == 2) {
            
            
            for(int i=0; i<12; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : list1){
                if(questionsBean.getType()==0)
                { 
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                } else if(questionsBean.getType()==1)
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++; 
                }else if(questionsBean.getType()==0 || questionsBean.getType()==1 )
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++; 
                } 
                
                 
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = list1.size() % 10;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
            
            // for new 
             mainAnswerSheet+=completeAnswerSheeet;
        String column1 = "cc|";
        String start1 = "\\begin{longtable}{|";
        String end1 = "\\end{longtable}";
        int index1 = 1;
            
            
            for(int i=0; i<7; i++)
                start1 += column1;
            start1 += "} \\hline";
            mainAnswerSheet += start1;
            for(QuestionBean questionsBean : list2){
             
                if(questionsBean.getType()==2)
                {
//                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    mainAnswerSheet += (startNo++) +"&"+ questionsBean.getNumericalAnswer()+""+questionsBean.getUnit();
                    if(index1 % 7 == 0 )
                        mainAnswerSheet += "\\hline";
                    else 
                        mainAnswerSheet +=  " & ";
                    index1++; 
                }  
            }
            if((index1-1) % 7 != 0) {
                mainAnswerSheet = mainAnswerSheet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = list2.size() % 7;
                mainAnswerSheet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end1;
            } else {
                mainAnswerSheet += end1;
            }
            
        } else {
            
            for(int i=0; i<12; i++)
                start += column;
            start += "} \\hline";
            completeAnswerSheeet += start;
            for(QuestionBean questionsBean : list1){
                if(questionsBean.getType()==0)
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }  else if(questionsBean.getType()==1)
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\\\ \\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                } else if(questionsBean.getType()==0 || questionsBean.getType()==1 )
                {
                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    if(index % 12 == 0 )
                        completeAnswerSheeet += "\\hline";
                    else 
                        completeAnswerSheeet +=  " & ";
                    index++;
                }
            }
            if((index-1) % 10 != 0) {
                completeAnswerSheeet = completeAnswerSheeet.substring(0, completeAnswerSheeet.length()-2);
                int modValue = selectedQuestionList.size() % 10;
                completeAnswerSheeet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end;
            } else {
                completeAnswerSheeet += end;
            }
            // for new 
                 mainAnswerSheet+=completeAnswerSheeet;
            
        String column1 = "cc|";
        String start1 = "\\begin{longtable}{|";
        String end1 = "\\end{longtable}";
        int index1 = 1;
            
            
            for(int i=0; i<7; i++)
                start1 += column1;
            start1 += "} \\hline";
            mainAnswerSheet += start1;
            for(QuestionBean questionsBean : list2){
             if(questionsBean.getType()==2)
                {
//                    completeAnswerSheeet += (startNo++) +"&"+ questionsBean.getAnswer();
                    mainAnswerSheet += (startNo++) +"&"+ questionsBean.getNumericalAnswer()+""+questionsBean.getUnit();
                    if(index1 % 7 == 0 )
                        mainAnswerSheet += "\\hline";
                    else 
                        mainAnswerSheet +=  " & ";
                    index1++;
                }
            }
            if((index1-1) % 7 != 0) {
                mainAnswerSheet = mainAnswerSheet.substring(0, mainAnswerSheet.length()-2);
                int modValue = list2.size() % 7;
                mainAnswerSheet += "\\\\ \\cline{1-"+(modValue*2)+"}"+ end1;
            } else {
                mainAnswerSheet += end1;
            }
            
        }
//        System.out.println(completeAnswerSheeet);
        return mainAnswerSheet;
    }
    public static void main(String[] args) {
        ArrayList<QuestionBean> quesList = new QuestionOperation().getQuestuionsChapterWise(27);
//        for(int i=1001;i<=86;i++)
//            quesList
        
        System.out.println("QUUS:" + new AnswerSheetProcessing().getAnswerSheet(quesList, 1001));
    }
    
}
